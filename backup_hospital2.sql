--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.5
-- Started on 2015-07-05 11:18:55

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 181 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1997 (class 0 OID 0)
-- Dependencies: 181
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 177 (class 1259 OID 16546)
-- Name: administracion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE administracion (
    id integer NOT NULL,
    descripcion_observacion character varying(255),
    fecha_administracion timestamp without time zone,
    cantidad_administracion numeric(10,5),
    unidades character varying(10),
    parte_cuerpo character varying(50),
    id_centro_asistencial integer,
    id_profesional integer,
    id_vacuna integer,
    eliminado boolean DEFAULT false,
    id_paciente integer,
    via character varying(20)
);


ALTER TABLE public.administracion OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 16544)
-- Name: administracion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE administracion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.administracion_id_seq OWNER TO postgres;

--
-- TOC entry 1998 (class 0 OID 0)
-- Dependencies: 176
-- Name: administracion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE administracion_id_seq OWNED BY administracion.id;


--
-- TOC entry 173 (class 1259 OID 16415)
-- Name: centro_asistencial; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE centro_asistencial (
    id integer NOT NULL,
    nombre character varying(200),
    direccion character varying(255),
    ciudad character varying(50),
    departamento character varying(30),
    eliminado boolean DEFAULT false
);


ALTER TABLE public.centro_asistencial OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 16413)
-- Name: centro_asistencial_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE centro_asistencial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.centro_asistencial_id_seq OWNER TO postgres;

--
-- TOC entry 1999 (class 0 OID 0)
-- Dependencies: 172
-- Name: centro_asistencial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE centro_asistencial_id_seq OWNED BY centro_asistencial.id;


--
-- TOC entry 180 (class 1259 OID 16588)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 16407)
-- Name: paciente; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE paciente (
    id integer NOT NULL,
    nombres character varying(100),
    apellidos character varying(100),
    ci integer
);


ALTER TABLE public.paciente OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 16405)
-- Name: paciente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE paciente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.paciente_id_seq OWNER TO postgres;

--
-- TOC entry 2000 (class 0 OID 0)
-- Dependencies: 170
-- Name: paciente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE paciente_id_seq OWNED BY paciente.id;


--
-- TOC entry 175 (class 1259 OID 16456)
-- Name: profesional; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE profesional (
    id integer NOT NULL,
    nombres character varying(100),
    apellidos character varying(100),
    ci character varying(20),
    nro_registro character varying(100)
);


ALTER TABLE public.profesional OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 16454)
-- Name: profesional_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE profesional_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profesional_id_seq OWNER TO postgres;

--
-- TOC entry 2001 (class 0 OID 0)
-- Dependencies: 174
-- Name: profesional_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE profesional_id_seq OWNED BY profesional.id;


--
-- TOC entry 179 (class 1259 OID 16580)
-- Name: vacuna; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vacuna (
    id integer NOT NULL,
    nombre_vacuna character varying(100),
    lote character varying(100),
    laboratorio_denominacion character varying(100),
    fecha_adquisicion timestamp without time zone,
    fecha_vencimiento timestamp without time zone,
    eliminado boolean DEFAULT false
);


ALTER TABLE public.vacuna OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 16578)
-- Name: vacuna_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vacuna_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vacuna_id_seq OWNER TO postgres;

--
-- TOC entry 2002 (class 0 OID 0)
-- Dependencies: 178
-- Name: vacuna_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE vacuna_id_seq OWNED BY vacuna.id;


--
-- TOC entry 1854 (class 2604 OID 16549)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administracion ALTER COLUMN id SET DEFAULT nextval('administracion_id_seq'::regclass);


--
-- TOC entry 1851 (class 2604 OID 16418)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY centro_asistencial ALTER COLUMN id SET DEFAULT nextval('centro_asistencial_id_seq'::regclass);


--
-- TOC entry 1850 (class 2604 OID 16410)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY paciente ALTER COLUMN id SET DEFAULT nextval('paciente_id_seq'::regclass);


--
-- TOC entry 1853 (class 2604 OID 16459)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY profesional ALTER COLUMN id SET DEFAULT nextval('profesional_id_seq'::regclass);


--
-- TOC entry 1856 (class 2604 OID 16583)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vacuna ALTER COLUMN id SET DEFAULT nextval('vacuna_id_seq'::regclass);


--
-- TOC entry 1986 (class 0 OID 16546)
-- Dependencies: 177
-- Data for Name: administracion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (2, 'No tuvo reaccion', '2016-01-05 00:00:00', 0.80000, 'ml', 'brazo derecho', 1, 1, 7, false, 14, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (23, 'No tuvo reaccion', '2016-03-07 00:00:00', 0.80000, 'ml', 'brazo izquierdo', 1, 10, 4, false, 5, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (24, 'Fiebre luego de aplicacion', '2016-05-08 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 8, 9, 9, false, 9, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (25, 'Fiebre luego de aplicacion', '2015-06-20 00:00:00', 0.80000, 'ml', 'brazo derecho', 9, 7, 7, false, 19, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (26, 'Fiebre luego de aplicacion', '2014-10-01 00:00:00', 0.70000, 'ml', 'brazo izquierdo', 2, 2, 7, false, 2, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (27, 'No tuvo reaccion', '2014-08-30 00:00:00', 0.70000, 'ml', 'brazo derecho', 11, 3, 6, false, 14, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (28, 'Fiebre luego de aplicacion', '2016-04-26 00:00:00', 0.80000, 'ml', 'brazo izquierdo', 14, 2, 2, false, 17, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (29, 'No tuvo reaccion', '2015-06-13 00:00:00', 1.00000, 'ml', 'intramuscular gluteo', 7, 5, 5, false, 6, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (30, 'Fiebre luego de aplicacion', '2016-04-01 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 14, 7, 5, false, 12, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (31, 'No tuvo reaccion', '2015-07-09 00:00:00', 1.00000, 'ml', 'brazo izquierdo', 10, 1, 3, false, 12, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (32, 'Fiebre luego de aplicacion', '2014-12-10 00:00:00', 0.50000, 'ml', 'brazo izquierdo', 8, 9, 10, false, 12, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (33, 'Fiebre luego de aplicacion', '2015-12-05 00:00:00', 1.00000, 'ml', 'brazo izquierdo', 11, 9, 8, false, 2, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (34, 'No tuvo reaccion', '2014-08-01 00:00:00', 0.50000, 'ml', 'brazo izquierdo', 14, 8, 8, false, 2, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (35, 'No tuvo reaccion', '2015-03-27 00:00:00', 0.80000, 'ml', 'brazo derecho', 7, 7, 5, false, 15, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (36, 'Fiebre luego de aplicacion', '2014-07-23 00:00:00', 1.00000, 'ml', 'intramuscular gluteo', 5, 4, 8, false, 1, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (37, 'Fiebre luego de aplicacion', '2015-10-02 00:00:00', 0.80000, 'ml', 'brazo izquierdo', 8, 10, 7, false, 18, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (38, 'No tuvo reaccion', '2015-05-19 00:00:00', 0.80000, 'ml', 'brazo izquierdo', 3, 7, 3, false, 15, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (39, 'No tuvo reaccion', '2016-01-19 00:00:00', 1.00000, 'ml', 'intramuscular gluteo', 8, 4, 5, false, 8, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (40, 'Fiebre luego de aplicacion', '2015-05-17 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 14, 10, 10, false, 4, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (41, 'Fiebre luego de aplicacion', '2015-07-23 00:00:00', 0.70000, 'ml', 'brazo izquierdo', 12, 8, 1, false, 10, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (42, 'Fiebre luego de aplicacion', '2014-06-29 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 14, 2, 10, false, 20, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (43, 'Fiebre luego de aplicacion', '2015-12-24 00:00:00', 0.50000, 'ml', 'brazo derecho', 2, 5, 2, false, 10, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (44, 'Fiebre luego de aplicacion', '2015-02-02 00:00:00', 0.70000, 'ml', 'brazo derecho', 7, 7, 6, false, 4, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (45, 'No tuvo reaccion', '2015-12-11 00:00:00', 0.50000, 'ml', 'brazo izquierdo', 4, 1, 1, false, 8, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (46, 'Fiebre luego de aplicacion', '2014-09-03 00:00:00', 0.70000, 'ml', 'brazo derecho', 12, 7, 10, false, 17, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (47, 'Fiebre luego de aplicacion', '2014-08-15 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 9, 5, 1, false, 5, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (48, 'No tuvo reaccion', '2014-10-13 00:00:00', 1.00000, 'ml', 'brazo izquierdo', 9, 9, 4, false, 6, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (66, 'No tuvo reaccion', '2016-01-06 00:00:00', 1.00000, 'ml', 'brazo derecho', 10, 1, 1, false, 4, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (67, 'No tuvo reaccion', '2015-12-13 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 5, 8, 7, false, 20, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (68, 'No tuvo reaccion', '2015-09-15 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 14, 6, 4, false, 10, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (69, 'Fiebre luego de aplicacion', '2015-10-16 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 14, 6, 4, false, 8, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (70, 'Fiebre luego de aplicacion', '2014-10-15 00:00:00', 0.80000, 'ml', 'brazo derecho', 13, 8, 10, false, 20, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (71, 'Fiebre luego de aplicacion', '2015-07-09 00:00:00', 1.00000, 'ml', 'brazo izquierdo', 14, 10, 5, false, 5, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (72, 'Fiebre luego de aplicacion', '2014-07-17 00:00:00', 0.80000, 'ml', 'brazo izquierdo', 2, 2, 6, false, 12, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (73, 'Fiebre luego de aplicacion', '2016-01-31 00:00:00', 0.50000, 'ml', 'brazo derecho', 13, 6, 4, false, 19, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (74, 'Fiebre luego de aplicacion', '2016-02-02 00:00:00', 0.50000, 'ml', 'brazo derecho', 2, 9, 5, false, 11, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (75, 'Fiebre luego de aplicacion', '2014-06-20 00:00:00', 0.70000, 'ml', 'brazo derecho', 4, 6, 6, false, 5, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (76, 'Fiebre luego de aplicacion', '2014-06-19 00:00:00', 0.70000, 'ml', 'brazo derecho', 3, 5, 10, false, 20, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (77, 'Fiebre luego de aplicacion', '2015-08-26 00:00:00', 1.00000, 'ml', 'brazo derecho', 6, 6, 8, false, 1, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (78, 'No tuvo reaccion', '2014-10-04 00:00:00', 1.00000, 'ml', 'brazo derecho', 12, 8, 2, false, 15, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (79, 'No tuvo reaccion', '2016-05-26 00:00:00', 0.80000, 'ml', 'brazo derecho', 3, 7, 5, false, 8, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (80, 'Fiebre luego de aplicacion', '2015-10-13 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 12, 9, 5, false, 16, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (81, 'Fiebre luego de aplicacion', '2014-09-29 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 8, 8, 6, false, 11, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (82, 'No tuvo reaccion', '2014-09-11 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 7, 1, 1, false, 10, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (83, 'No tuvo reaccion', '2015-07-26 00:00:00', 0.50000, 'ml', 'brazo derecho', 13, 10, 6, false, 12, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (84, 'No tuvo reaccion', '2014-09-28 00:00:00', 0.80000, 'ml', 'brazo izquierdo', 13, 9, 9, false, 6, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (85, 'Fiebre luego de aplicacion', '2015-05-12 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 7, 10, 1, false, 7, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (86, 'No tuvo reaccion', '2016-02-25 00:00:00', 1.00000, 'ml', 'brazo derecho', 13, 10, 3, false, 5, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (87, 'No tuvo reaccion', '2014-12-16 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 9, 8, 6, false, 9, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (88, 'Fiebre luego de aplicacion', '2016-04-01 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 11, 9, 4, false, 15, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (89, 'No tuvo reaccion', '2016-01-23 00:00:00', 0.70000, 'ml', 'brazo derecho', 5, 9, 10, false, 10, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (90, 'Fiebre luego de aplicacion', '2014-10-28 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 10, 5, 9, false, 14, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (91, 'Fiebre luego de aplicacion', '2016-05-12 00:00:00', 0.70000, 'ml', 'brazo derecho', 3, 6, 5, false, 4, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (92, 'Fiebre luego de aplicacion', '2014-11-15 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 7, 4, 9, false, 15, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (93, 'No tuvo reaccion', '2015-08-17 00:00:00', 1.00000, 'ml', 'brazo izquierdo', 6, 9, 7, false, 14, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (94, 'Fiebre luego de aplicacion', '2014-12-16 00:00:00', 0.70000, 'ml', 'brazo derecho', 7, 5, 4, false, 4, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (95, 'Fiebre luego de aplicacion', '2015-11-21 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 10, 10, 9, false, 8, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (96, 'No tuvo reaccion', '2015-07-25 00:00:00', 0.50000, 'ml', 'brazo izquierdo', 12, 6, 5, false, 13, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (97, 'No tuvo reaccion', '2015-11-07 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 2, 1, 1, false, 7, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (98, 'No tuvo reaccion', '2016-05-22 00:00:00', 0.80000, 'ml', 'brazo izquierdo', 4, 6, 2, false, 4, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (99, 'Fiebre luego de aplicacion', '2016-01-09 00:00:00', 0.80000, 'ml', 'brazo izquierdo', 5, 6, 3, false, 3, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (100, 'No tuvo reaccion', '2015-11-14 00:00:00', 0.80000, 'ml', 'brazo derecho', 5, 2, 3, false, 9, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (1, 'Fiebre luego de aplicacion', '2014-06-25 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 7, 8, 5, false, 17, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (3, 'No tuvo reaccion', '2015-05-21 00:00:00', 0.50000, 'ml', 'brazo derecho', 2, 2, 3, false, 2, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (4, 'Fiebre luego de aplicacion', '2016-02-13 00:00:00', 1.00000, 'ml', 'brazo izquierdo', 2, 9, 8, false, 19, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (5, 'No tuvo reaccion', '2015-12-15 00:00:00', 0.50000, 'ml', 'intramuscular gluteo', 2, 3, 3, false, 2, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (6, 'No tuvo reaccion', '2015-01-16 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 6, 9, 10, false, 3, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (7, 'No tuvo reaccion', '2015-08-23 00:00:00', 0.80000, 'ml', 'brazo izquierdo', 1, 3, 6, false, 2, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (8, 'No tuvo reaccion', '2014-07-29 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 5, 3, 4, false, 12, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (9, 'Fiebre luego de aplicacion', '2015-12-06 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 12, 8, 7, false, 2, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (10, 'No tuvo reaccion', '2016-02-09 00:00:00', 0.80000, 'ml', 'brazo derecho', 13, 4, 5, false, 3, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (11, 'Fiebre luego de aplicacion', '2014-10-07 00:00:00', 0.70000, 'ml', 'intramuscular gluteo', 7, 3, 7, false, 11, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (12, 'No tuvo reaccion', '2015-11-01 00:00:00', 0.50000, 'ml', 'brazo izquierdo', 10, 7, 10, false, 14, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (13, 'No tuvo reaccion', '2015-10-07 00:00:00', 0.70000, 'ml', 'brazo izquierdo', 14, 9, 5, false, 20, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (14, 'No tuvo reaccion', '2015-03-10 00:00:00', 0.80000, 'ml', 'brazo derecho', 5, 7, 3, false, 1, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (15, 'No tuvo reaccion', '2014-08-27 00:00:00', 0.50000, 'ml', 'brazo derecho', 12, 4, 8, false, 17, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (16, 'Fiebre luego de aplicacion', '2014-11-02 00:00:00', 1.00000, 'ml', 'brazo derecho', 8, 4, 9, false, 7, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (17, 'Fiebre luego de aplicacion', '2015-09-07 00:00:00', 1.00000, 'ml', 'intramuscular gluteo', 2, 1, 5, false, 8, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (18, 'No tuvo reaccion', '2014-07-01 00:00:00', 0.50000, 'ml', 'intramuscular gluteo', 1, 4, 10, false, 17, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (19, 'No tuvo reaccion', '2014-06-24 00:00:00', 0.70000, 'ml', 'brazo izquierdo', 10, 4, 2, false, 16, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (20, 'No tuvo reaccion', '2015-05-28 00:00:00', 0.50000, 'ml', 'intramuscular gluteo', 10, 6, 4, false, 1, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (21, 'Fiebre luego de aplicacion', '2015-12-11 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 8, 7, 9, false, 8, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (22, 'Fiebre luego de aplicacion', '2014-07-05 00:00:00', 0.80000, 'ml', 'brazo izquierdo', 10, 6, 9, false, 17, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (49, 'No tuvo reaccion', '2015-06-21 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 7, 5, 10, false, 19, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (50, 'No tuvo reaccion', '2016-05-04 00:00:00', 1.00000, 'ml', 'intramuscular gluteo', 2, 10, 5, false, 15, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (51, 'No tuvo reaccion', '2014-12-01 00:00:00', 0.80000, 'ml', 'intramuscular gluteo', 13, 10, 4, false, 15, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (52, 'No tuvo reaccion', '2015-01-25 00:00:00', 1.00000, 'ml', 'brazo izquierdo', 14, 6, 6, false, 2, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (53, 'Fiebre luego de aplicacion', '2014-10-01 00:00:00', 1.00000, 'ml', 'brazo derecho', 8, 1, 9, false, 10, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (54, 'No tuvo reaccion', '2015-08-22 00:00:00', 0.70000, 'ml', 'brazo izquierdo', 14, 1, 6, false, 2, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (55, 'Fiebre luego de aplicacion', '2014-11-20 00:00:00', 1.00000, 'ml', 'brazo izquierdo', 8, 4, 5, false, 20, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (56, 'Fiebre luego de aplicacion', '2015-05-23 00:00:00', 0.70000, 'ml', 'brazo izquierdo', 11, 5, 4, false, 1, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (57, 'No tuvo reaccion', '2016-02-27 00:00:00', 0.50000, 'ml', 'brazo derecho', 8, 9, 3, false, 1, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (58, 'Fiebre luego de aplicacion', '2015-06-29 00:00:00', 1.00000, 'ml', 'intramuscular gluteo', 1, 7, 1, false, 20, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (59, 'Fiebre luego de aplicacion', '2014-10-11 00:00:00', 1.00000, 'ml', 'intramuscular gluteo', 3, 9, 1, false, 9, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (60, 'No tuvo reaccion', '2015-01-05 00:00:00', 0.50000, 'ml', 'intramuscular gluteo', 9, 4, 3, false, 17, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (61, 'Fiebre luego de aplicacion', '2015-12-27 00:00:00', 1.00000, 'ml', 'brazo izquierdo', 1, 5, 1, false, 2, 'INTRAMUSCULAR');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (62, 'No tuvo reaccion', '2015-04-07 00:00:00', 0.70000, 'ml', 'brazo derecho', 6, 2, 7, false, 2, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (63, 'Fiebre luego de aplicacion', '2016-01-28 00:00:00', 0.50000, 'ml', 'brazo derecho', 2, 9, 9, false, 17, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (64, 'No tuvo reaccion', '2014-09-06 00:00:00', 0.70000, 'ml', 'brazo izquierdo', 13, 6, 6, false, 13, 'SUBCUTANEA');
INSERT INTO administracion (id, descripcion_observacion, fecha_administracion, cantidad_administracion, unidades, parte_cuerpo, id_centro_asistencial, id_profesional, id_vacuna, eliminado, id_paciente, via) VALUES (65, 'Fiebre luego de aplicacion', '2014-12-07 00:00:00', 1.00000, 'ml', 'intramuscular gluteo', 11, 2, 2, false, 3, 'SUBCUTANEA');


--
-- TOC entry 2003 (class 0 OID 0)
-- Dependencies: 176
-- Name: administracion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('administracion_id_seq', 100, true);


--
-- TOC entry 1982 (class 0 OID 16415)
-- Dependencies: 173
-- Data for Name: centro_asistencial; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (1, 'Centro Medico La Costa', 'Avenida General José Gervasio Artigas 1500', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (2, 'Hospital Nacional de Itaugua', 'Ruta Gral. Marcial Samaniego Km 6', 'ITAUGUA', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (3, 'Centro Médico Bautista', 'República Argentina esquina Campos Cervera.', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (4, 'Hospital de Clínicas', 'Avda. Mcal. López', 'SAN LORENZO', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (5, 'Sanatorio San Roque', 'Eligio Ayala 1383', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (6, 'Sanatorio Santa Clara', 'Avda Rodriguez de Francia Nº 910 ', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (7, 'Sanatorio Migone-Battilana', 'Eligio Ayala 1293', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (8, 'Sanatorio Italiano', 'Dr. Luis Zanotti Cavazzoni y Av. España', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (9, 'Sanatorio Adventista', 'Pettirossi 380 c/ Pai Perez', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (10, 'Policlínico Policial Rigoberto Caballero', 'Mcal. López y Kubitschek ', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (11, 'Instituto de Previsión Social (IPS)', 'Av. Stmo. Sacramento Y Dr. Manuel Peña', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (12, 'Hospital San Pablo', 'Av. De La Victoria e Incas ', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (13, 'Hospital San Jorge', 'Madame Linch y Linea Ferrea (Ex Caballeria)', 'ASUNCION', 'CENTRAL', false);
INSERT INTO centro_asistencial (id, nombre, direccion, ciudad, departamento, eliminado) VALUES (14, 'Hospital Materno Infantil de San Lorenzo', 'Dr. Gabriel Pellón', 'SAN LORENZO', 'CENTRAL', false);


--
-- TOC entry 2004 (class 0 OID 0)
-- Dependencies: 172
-- Name: centro_asistencial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('centro_asistencial_id_seq', 14, true);


--
-- TOC entry 2005 (class 0 OID 0)
-- Dependencies: 180
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hibernate_sequence', 1, false);


--
-- TOC entry 1980 (class 0 OID 16407)
-- Dependencies: 171
-- Data for Name: paciente; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (1, 'ENRIQUE BORDON', NULL, 384845);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (2, 'MIRTA AMARILLA', NULL, 721912);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (3, '1654321', NULL, 1654321);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (4, 'ABRAHAN', 'CAÑETE VILLALBA', 3830290);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (5, 'ROBERTO SANTANDER', NULL, 3197437);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (6, 'JOSE E. GIMENEZ C.', NULL, 650915);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (7, 'PEDRO', 'PEREZ', 123456);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (8, 'FATIMA MARIA', 'AMARILLA RIVAS', 1279617);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (9, 'BLANCA ESTELA', 'ROMERO DE INSAURRALDE', 378218);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (10, 'LAURA CAROLINA', 'SANTANDER', 1842194);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (11, 'GUSTAVO ALFONSO', 'PETTENGIL BAREIRO', 1121080);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (12, 'WILBERTO', 'OTAZU FRANCO', 1508996);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (13, 'BLANCA AZUCENA', 'DELGADO PAREDES', 1593178);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (14, 'BLANCA IRENE', 'FERNANDEZ', 486649);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (15, 'CARMEN MARGARITA', 'AYALA OCAMPOS', 3819965);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (16, 'DORA MARITHE', 'DURE  DE ALMADA', 867904);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (17, 'FEDERICO ANIBAL ', 'EMERY', 1397868);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (18, 'EPIFANIA', 'DELVALLE ORTIZ', 853420);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (19, 'DIEGO JAVIER', 'ZELAYA RODRIGUEZ', 2864873);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (20, 'EVA MARGARITA', 'ESQUIVEL GONZALEZ', 2151557);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (21, 'PABLO CESAR', 'CANO GONZALEZ', 3438571);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (22, 'CAROLINA NOELIA', 'RODRIGUEZ', 1403053);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (23, 'EDGAR ', 'QUEIROZ', 3412900);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (24, 'ALEJANDRA ERNESTA', 'ORTIZ DICK', 3982947);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (25, 'CARLOS ALEXIS', 'PAREDES ASTIGARRAGA', 1340143);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (26, 'DIANA BELEN ', 'ORTIZ CABALLERO', 4784004);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (27, 'PEDRO IVAN', 'DUARTE', 2252785);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (28, 'ODINA ELIZABETH', 'JACOBO GIMENEZ', 1210694);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (29, 'ALDO JOSE', 'BOGADO FERNANDEZ', 995837);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (30, 'EDGAR RAMON', 'MARTINEZ', 3543846);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (31, 'ROBERTO LUIS', 'BEJARANO PINEDA', 723998);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (32, 'ROMILIO LUIS', 'COLUNGA CABALLERO', 511670);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (33, 'ROSALINA', 'BENITEZ DE VILLALBA', 1068422);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (34, 'RUBEN DARIO', 'FERNANDEZ YURTZ', 920971);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (35, 'EDGAR RAMON', 'OSORIO GONZALEZ', 2981134);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (36, 'EMILIO RICARDO', 'PERALTA', 3281236);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (37, 'EPIFANIO', 'ARCE VELAZQUEZ', 1259888);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (38, 'ERNESTO JOSE', 'BARCHELLO BOTTINO', 3860229);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (39, 'EXPIDIO', 'PALACIOS', 845265);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (40, 'FIDENCIO RUVILDO', 'OZUNA CHAVEZ', 2226612);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (41, 'GUSTAVO DANIEL', 'MONGES ARZAMENDIA', 3967480);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (42, 'PEDRO RIGOBERTO ', 'OLMEDO SEMIDEY', 660161);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (43, 'ANA CLAUDIA ', 'RECALDE MASSI', 4118470);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (44, 'JULIO CESAR', 'PINTOS ', 4308260);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (45, 'LUIS MIGUEL', 'ARCE RODRIGUEZ', 1756888);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (46, 'JUANA ELIZABETH', 'CHAVEZ ROMERO', 3393870);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (47, 'JULIO CESAR', 'AYALA', 2080604);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (48, 'JULIO CESAR', 'BOGARIN FERNANDEZ', 930215);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (49, 'JULIO CESAR', 'ARECO CESPEDES', 1189919);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (50, 'LYDIA RAQUEL', 'RIVEROS TORRES', 645512);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (51, 'NERY GABRIEL', 'SANTOS MOLINIER', 1528277);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (52, 'DENICE NORBE', 'CANO DE ASERETTO', 743034);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (53, 'ENRIQUE ANDRES', 'GONZALEZ', 1166684);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (54, 'ESTEBAN', 'OPORTO', 769662);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (55, 'FABIAN CALIXTO', 'CAMACHO SANTACRUZ', 2057141);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (56, 'FABIO ALBERTO', 'CORONEL RAMIREZ', 4817644);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (57, 'GREGORIA', 'UDRIZAR FERNANDEZ', 440941);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (58, 'GUSTAVO AMADO', 'VALDEZ LOPEZ', 1063475);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (59, 'ROGELIO MIGUEL ANGEL', 'OVEJERO FERNANDEZ', 780443);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (60, 'VIRINO ', 'DURE MORINIGO', 1073067);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (61, 'AGUSTIN', 'GIMENEZ RIVAS', 2892980);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (62, 'AMANDA LUCIA', 'INSFRAN OCAMPOS', 1510098);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (63, 'SINDULFO ', 'BARRIOS', 668983);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (64, 'MYRIAM ROSA', 'CHAMORRO', 800841);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (65, 'CELSO ALEJANDRO', 'BAREIRO RECALDE', 649824);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (66, 'MARGARITA', 'WOOD', 448797);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (67, 'PAOLA TAMARA', 'CUEVAS', 3562881);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (68, 'CRISTHIAN RAFAEL', 'ARGUELLO CARMONA', 4829763);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (69, 'YOLANDA', 'VAZQUEZ', 403007);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (70, 'CAMILO NICOLAS', 'BAEZ GRACIA', 4250872);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (71, 'JUANA MIRTA GIMENEZ', NULL, 764401);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (72, 'CESAR FERNANDEZ', NULL, 853405);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (73, 'HECTOR PAREDES', NULL, 2319645);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (74, 'PERLA LEZCANO', NULL, 2053358);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (75, 'CARLOS BENITEZ', NULL, 932809);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (76, 'ENRIQUE VIDAL', 'GARCIA GIMENEZ', 441595);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (77, 'GORGE DAVID', 'ALVARENGA MARTINEZ', 1182677);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (78, 'JUAN CARLOS ', 'MATTO MAIDANA', 1000119);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (79, 'FABIAN ANDRES', 'ROTELA', 5023539);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (80, 'JORGE ', 'SOSA', 3656866);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (81, 'GUZMAN ARIEL', 'MARTINEZ SOTO', 1502402);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (82, 'JORGE VICENTE', 'AGUAYO MOLINAS', 650970);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (83, 'MARIA LORENA', 'MENDEZ DE GUSTAFSON', 766742);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (84, 'JUANA DE DIOS', 'BUSTAMANTE', 2239429);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (85, 'LAURA GRACIELA', 'BENITEZ LEON', 803415);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (86, 'JORGE DANIEL', 'OZORIO VERA', 2204619);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (87, 'GUSTAVO SOTERO ULISES', 'RODRIGUEZ BAEZ', 979779);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (88, 'JOSE LUIS', 'LANERI MENDOZA', 316085);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (89, 'REINALDO', 'PERALTA AQUINO', 814976);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (90, 'BARBARA LORENA', 'RUIZ DIAZ BARRETO', 2061117);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (91, 'BERNARDINO ENRIQUE ', 'CABALLERO BARRIOS', 810815);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (92, 'CARLOS ANIBAL', 'CANTERO CHENA', 3614681);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (93, 'FANY CAROLINA', 'LOPEZ CHAVEZ', 4360847);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (94, 'HAZEL RACHEL ANN', 'PARAS OZORIO', 2488140);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (95, 'MARIA IRENE', 'ALVAREZ VERA', 738685);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (96, 'MARIA ROSANA', 'GIMENEZ', 3538494);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (97, 'IRMA BEATRIZ ', 'MENCIA OSORIO', 3290542);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (98, 'BLAS DANIEL', 'GINI', 2593093);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (99, 'LAURA LILIANA', 'MINARDI INSFRAN', 2193055);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (100, 'CLAUDIA ', 'CABALLERO', 2189871);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (101, 'DARIO EUSEBIO', 'CARDOZO CORONEL', 3411344);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (102, 'LAURA ANSELMA', 'RODRIGUEZ', 1854689);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (103, 'LAURA LETICIA', 'OJEDA', 2144925);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (104, 'FLORENCIO CABRERA', NULL, 804521);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (105, 'EDGAR ANIBAL ALONSO ROA', NULL, 2825800);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (106, 'MARIO A. JACQUES MARTIN', NULL, 3027176);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (107, 'CRISTINA ESTELA LARAN', NULL, 1548947);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (108, 'CARLOS GOROSTIAGA', NULL, 3337868);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (109, 'RUBEN AYALA BOGADO', NULL, 1301978);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (110, 'MICAL RODRIGUEZ', NULL, 4178952);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (111, 'ARTURO FABIAN ALMADA', NULL, 3515894);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (112, '2370956', NULL, 2370956);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (113, 'JOSE FELICIANO', 'DECCLESIIS DUARTE', 1750553);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (114, 'HANNELORE', 'GOTZL VDA DE ENCINA', 3414309);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (115, 'ESTELA MARY', 'VERA', 1199769);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (116, 'LISA NATALIA', 'OVIEDO BENITEZ', 2460887);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (117, 'LUCIANA', 'ARAUJO ZARZA', 540279);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (118, 'MARCOS ANTONIO', 'BARRETO MENDIETA', 1843145);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (119, 'MARGARITA', 'PEÑA CONCHA', 540044);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (120, 'MARIA DE LOURDES', 'FRANCO DE UGARTE', 1099060);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (121, 'MARIA ELENA', 'FRUTOS VERA', 2996775);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (122, 'MARIA EMILIA', 'ALVAREZ LEZCANO', 3660319);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (123, 'MARIA LIZ BEATRIZ', 'GIMENEZ DE COLMAN', 809928);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (124, 'MARIA LOURDES', 'ACOSTA MEZA', 1216949);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (125, 'MARIELA LARISA ', 'SANABRIA BENITEZ', 809483);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (126, 'MARIO RAMON', 'ROMAN BARRETO', 3713145);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (127, 'MARTA PATRICIA', 'DE LOS RIOS GONZALEZ', 3532869);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (128, 'MARTHA CATARINA', 'GARCIA ALVES', 2484917);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (129, 'MARTIN FEDERICO', 'NARVAEZ', 4075770);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (130, 'MERCEDES ELIZABETH ', 'ZORRILLA IRALA', 664330);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (131, 'MIGUEL ANGEL', 'CALDERON RUIZ', 662627);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (132, 'MIRNA', 'RAMIREZ VALENZUELA', 3392341);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (133, 'MIRTHA MARIA ', 'PEREZ DE OVELAR', 623829);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (134, 'MODESTO', 'MONGELOS PEREIRA', 2313471);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (135, 'MONICA ROSSANNA', 'MORENO DE BURIAN', 1674760);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (136, 'MYRIAN VICTORIA', 'FLOR CUEVAS', 1123439);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (137, 'MARTA CAROLINA', 'VELAZQUEZ SALDIVAR', 2357506);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (138, 'JORGE BERNAL', NULL, 3397491);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (139, 'ANTONIO MARIA FERREIRA', NULL, 366730);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (140, 'LORENA ROLON MARTINEZ', NULL, 1527837);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (141, 'JOSE ALFIRIO LESME GONZ', NULL, 2979495);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (142, 'RUBEN SILVA RAMIREZ', NULL, 1181851);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (143, 'HILDA R. BARRIOS', NULL, 3585244);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (144, 'GLORIA CONCEPCION GALEA', NULL, 714895);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (145, '368039', NULL, 368039);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (146, 'LOURDES MARGARITA', 'GOMEZ ORTIZ', 2859155);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (147, 'ANTONIO', 'INSFRAN PEREIRA', 1300126);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (148, 'CAROLINA ELIZABETH', 'ISASI NOGUERA', 1741570);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (149, 'FATIMA ELENA', 'MANCIA ASTA', 717040);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (150, 'FELIX AGUSTIN', 'ACOSTA AMARILLA', 3449365);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (151, 'GUSTAVO ALFREDO', 'LEITE GUSINKY', 489476);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (152, 'MANUEL ESTEBAN', 'LEGUIZAMON INSFRAN', 450472);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (153, 'MARIA TERESA', 'GONZALEZ VILLAMAYOR', 422117);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (154, 'MIRNA', 'QUIÑONEZ', 1507916);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (155, 'MARIA LAURA ', 'MARTINEZ PRANTTE ', 1173225);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (156, 'NELIDA ANTONIA ', 'FERREIRA MEZA', 1427508);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (157, 'PATRICIA ', 'MARTINEZ TRINIDAD', 900410);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (158, 'MARIA LUJAN', 'OJEDA CHAMORRO', 2915936);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (159, 'MARCELO DANIEL', 'BAEZ ESTIGARRIBIA', 1492296);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (160, 'NILDA GRACIELA', 'FLECHA SEITZ', 2971965);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (161, 'MARISA', 'MACORITTO D''ECCLESSIS', 2532134);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (162, 'EDUARDO ESTEBAN', 'LOPEZ MARTINEZ', 807559);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (163, 'CESAR AUGUSTO', 'NUÑEZ FIGUEREDO', 2099297);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (164, 'ALDO ANDRES', 'ROLON ULDERA', 4783377);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (165, 'DIEGO ALEJANDRO GARCIA', NULL, 1788594);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (166, 'MARIO INSFRAN', NULL, 685010);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (167, 'ROBERTO RAUL RODRIGUEZ', NULL, 4141662);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (168, 'JUAN DIONISIO PRIETO FR', NULL, 650961);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (169, 'CARLOS CASTRO', NULL, 697689);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (170, 'PATRICIA CAROLINA SALDI', NULL, 3802943);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (171, 'EMILCE M. RODRIGUEZ', NULL, 3955968);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (172, 'VERONICA DUARTE DOMANIC', NULL, 2835764);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (173, 'MIRTHA CECILIA MONTIEL', NULL, 1360489);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (174, 'LUCILA OVIEDO GOMEZ', NULL, 869239);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (175, 'CRISTINA ZUNILDA AVALOS', NULL, 4041206);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (176, 'SANTIAGO DOS SANTOS', NULL, 636404);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (177, 'MARTIN', 'ACOSTA MARTINEZ', 3206303);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (178, 'LIZA JANET ', 'AYALA GIMENEZ', 1280531);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (179, 'MARIA ELENA DE SANTOS', 'GARCIA', 748558);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (180, 'MARTIN ALCIDES', 'MARECO', 3505156);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (181, 'LUIS GUILLERMO', 'BENITEZ', 381377);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (182, 'JORGE ANDRES', 'BARRETO MENDIETA', 1843155);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (183, 'GUSTAVO RENE ', 'CENTURION VALDEZ', 2584592);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (184, 'PEDRO ALBERTO', 'MARTINEZ PALACIOS', 3525337);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (185, 'PEDRO GASPAR', 'DINATOR SUAZO', 6217659);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (186, 'EMILIO JOSE', 'GOMEZ FIGUEREDO', 2432169);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (187, 'JAVIER AGUADA RODRIGUEZ', NULL, 1206511);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (188, 'BENIGNO SANABRIA', NULL, 616024);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (189, 'MIRIAN RAMONA', 'LEZCANO BAEZ', 2854479);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (190, 'MIRTA CARDENAS', NULL, 3637375);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (191, 'CLAUDIA DENIS', NULL, 2996692);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (192, 'NELSON R. BITTAR', NULL, 922373);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (193, 'DELIA MARIA VIELMAN', NULL, 2319285);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (194, 'JOSE AUGUSTO LEON BENIT', NULL, 2148295);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (195, 'NELSON FABIAN ORTIZ', NULL, 3443002);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (196, '200', NULL, 200);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (197, 'RAMONA BAZAN', NULL, 683298);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (198, 'CLAUDIO A. GARCIA', NULL, 4225877);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (199, 'FRANCISCO IBERBUDEN', NULL, 935630);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (200, 'CYNTHIA DELGADO VELAZCO', NULL, 4222633);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (201, '3205178', NULL, 3205178);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (202, '629338', NULL, 629338);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (203, 'VERONICA GONZALEZ', NULL, 411999);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (204, 'ARMANDO', 'CARRILLO CACERES', 744963);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (205, 'ELMAR', 'DEGGELLER HORN', 2865791);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (206, 'DARIO ROBERTO', 'PORTILLO CAÑETE', 2290338);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (207, 'BLANCA SOLEDAD', 'AQUINO', 2346443);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (208, 'CARLOS ALBERTO', 'DAVALOS ZARATE', 3642987);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (209, 'VICTOR LUIS', 'BERNAL LUGO', 1267768);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (210, 'CARMEN ILUMINADA', 'ARECO CESPEDES', 571305);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (211, 'CESAR SANDRO', 'SALINAS SILGUERO', 2915264);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (212, 'ANA MARLENE', 'MARTINEZ CACERES', 3455219);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (213, 'DIEGO ARMANDO', 'GONZALEZ GIMENEZ', 3623961);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (214, 'DOMINGO ANTONIO', 'NOGUEZ COLARTE', 610728);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (215, 'DOMINGO GUZMAN', 'MACHUCA SALINAS', 1312781);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (216, 'EMILIA CONCEPCION', 'VEGA', 906694);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (217, 'GABRIEL ', 'CARRERA VALDEZ', 2125343);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (218, 'CRISTIAN ADALBERTO', 'VERA', 4517073);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (219, 'JORGE GUILLERMO ', 'AVALOS PORRO', 2552107);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (220, 'LISANDRO RENE', 'CENTURION', 2287714);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (221, 'LUCAS ATILIO', 'SOTOMAYOR LLANO', 2304026);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (222, 'LIZ VIRGINIA', 'CHAMORRO BERNADET', 2947374);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (223, 'CLAUDIA MARIA PATRICIA', 'DACAK SIENRA', 1121507);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (224, 'JORGE ALBERTO', 'FERRARO MIRANDA', 3531483);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (225, 'JORGE NICOLAS', 'CABRERA MARECO', 654407);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (226, 'JUAN MANUEL', 'CABRAL MONZON', 3513432);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (227, 'KARINA GABRIELA', 'CASAMADA ROJAS', 2880685);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (228, 'DEISY PATRICIA', 'VERA ROTELA', 3527392);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (229, 'ANALIDA MELCHORA', 'GIMENEZ DE GONZALEZ', 1986867);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (230, 'CAYO EFREN', 'CASTELL FERREIRA', 504150);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (231, 'JORGE SALVADOR BENITEZ', NULL, 2099520);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (232, 'JUAN ANGEL CARDENAS', NULL, 945764);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (233, 'GLORIA CELESTE', 'AMARILLA CARDOZO', 1108848);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (234, 'JORGE BENICIO', 'CABALLERO BAREIRO', 881117);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (235, 'JULIO CESAR', 'CANTERO FLEITAS', 3758181);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (236, 'LUIS FERNANDO', 'CACERES MALUF', 761659);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (237, 'LUISA PAOLA', 'PRANTTE ROJAS', 4014023);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (238, 'MARIA DEL CARMEN', 'CAJE DE RODRIGUEZ', 3519334);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (239, 'MARIA ELIZABETH', 'BRIZUELA', 3856629);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (240, 'MARIO FEDERICO', 'CATTONI RAMIREZ', 609428);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (241, 'MERCEDES RAQUEL', 'CUANDU CACERES', 2022215);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (242, 'ADRIAN ARNALDO', 'SANABRIA GONZALEZ', 1428794);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (243, 'AUGUSTO ALEJANDRO', 'ROMERO OLIVEIRA', 2523586);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (244, 'AVELINO ', 'GIMENEZ CORONEL', 3019749);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (245, 'ELIZABETH', 'SANCHEZ DE FRANCO', 3803858);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (246, 'EPIFANIO', 'RUIZ', 2450934);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (247, 'EVER', 'SEGOVIA MEDINA', 4029026);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (248, 'OSCAR GABRIEL', 'ROA', 2580526);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (249, 'FATIMA AVELINA', 'BENITEZ PAREDES', 3404704);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (250, 'FERNANDO ARIEL', 'CARDUS SILVERA', 1708471);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (251, 'MARIO ', 'MONGES', 2005920);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (252, 'GUSTAVO RAMON', 'GAMARRA DUARTE', 2439472);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (253, 'DENISE', 'TREVISAN TROCHE', 1420937);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (254, 'ANDRES LUIS', 'ORLANDINI CACERES', 429018);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (255, 'MARTA BEATRIZ ', 'VILLALBA VAZQUEZ', 3219663);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (256, 'JORGE ', 'JIMENEZ NIELSEN', 2206726);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (257, 'JOSE ATILANO', 'GAUTO CABRERA', 661459);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (258, 'JOSE MARIA', 'RIOS ROMAN', 3489553);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (259, 'JUAN PABLO ', 'ESTIGARRIBIA L.', 2209025);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (260, 'JULIO CESAR', 'IRALA VILLAR', 1093549);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (261, 'MIGUEL ANGEL', 'SALCEDO ESPINOLA', 328982);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (262, '1391261', NULL, 1391261);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (263, 'OSCAR PEDRO', 'CACERES ZOTTI', 639586);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (264, 'ANDREA ROJAS OJEDA', NULL, 3652712);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (265, 'EDGAR RAMON MACCHI RODR', NULL, 656669);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (266, 'FELIPE SILVERIO', 'RODRIGUEZ MEDINA', 738543);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (267, 'FELIX', 'GONZALEZ', 785564);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (268, 'GLADYS ZUNILDA DEJESUS', 'GAMARRA DE OCAMPO', 573463);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (269, 'GLORIA BEATRIZ', 'ROJAS DE GIMENEZ', 1257699);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (270, 'GUSTAVO ANIBAL', 'TOLEDO IRRAZABAL', 3300552);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (271, 'HIPOLITA', 'GONZALEZ', 453872);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (272, 'JORGE DANIEL', 'ROA', 1278623);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (273, 'JORGE RANULFO', 'CHAPARRO FRANCO', 1998892);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (274, 'JOSE MIGUEL', 'SERVIAN SANABRIA', 3793382);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (275, 'ELIEZER', 'ORTIZ CHAVEZ', 3391471);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (276, 'FRANCISCO', 'OJEDA CONTRERAS', 3234281);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (277, 'JOSE NERY', 'MARIÑO GALIANO', 421555);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (278, 'JUAN ADOLFO', 'AYALA GONZALEZ', 567478);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (279, 'NANCY ANGELICA', 'BENITEZ ROMAN', 818369);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (280, 'NELSON ARCADIO', 'RIVERA ANTUNEZ', 395182);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (281, 'NELSON RAMON', 'FLORENTIN MARTINEZ', 3488904);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (282, 'NESTOR RUBEN', 'RUIZ', 2513085);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (283, 'NILDA INOCENCIA', 'PEREIRA CABRERA', 365815);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (284, 'NIMIA SALVADORA', 'GUERRERO MEDINA', 4126405);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (285, 'NOELIA ALEXANDRA', 'TORALES RIVERA', 3232315);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (286, 'PABLO', 'CUEVAS GIMENEZ', 805214);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (287, 'NAIDA FABIOLA', 'ALDERETE DUARTE', 3701371);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (288, 'NIKOLAUS STEFAN', 'OSIW', 1936894);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (289, 'DIEGO MATEO', 'PEYRAT FLORENTIN', 1545097);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (290, 'LUIS ALBERTO', 'MENDOZA GONZALEZ', 741476);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (291, 'NILSON ELADIO FLEITAS', NULL, 1345739);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (292, 'OLGA TELESFORA', 'GONZALEZ DE ARELLANO', 2223768);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (293, 'OSCAR JAVIER', 'ORTEGA MONZON', 1711872);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (294, 'OSCAR', 'STARK ROBLEDO', 743000);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (295, 'OSVALDO DAVID', 'MARIÑO RIVAS', 3212255);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (296, 'PABLO DANIEL ', 'LLAMAS BENITEZ', 2142980);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (297, 'PASCASIO MARTIN', 'ORUE GAMARRA', 484148);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (298, 'PATRICIA MARLENE', 'SERVIAN', 1136815);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (299, 'PATROCINIA IRENE', 'PRESENTADO DE CHAMORRO', 1162128);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (300, 'RAFAEL', 'BARRIOS PRANTTE', 1076070);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (301, 'RAMON', 'RODRIGUEZ', 758684);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (302, 'RENE GUSTAVO', 'MELGAREJO BORDON', 1248079);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (303, 'ROBERTO', 'NUÑEZ AYALA', 2462206);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (304, 'ROBERTO ELADIO', 'BERNAL HERMOSA', 609935);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (305, 'ROBERTO ENRIQUE JOSE', 'MACHUCA RODRIGUEZ', 660710);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (306, 'ROCIO MONSERRAT', 'SEGOVIA MARSA', 1886982);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (307, 'RODNEY DAVID', 'APODACA PAREDES', 1493799);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (308, 'RODOLFO AURELIO', 'FERREIRA RUIZ DIAZ', 458523);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (309, 'ROSMERY', 'ARGAÑA', 3775618);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (310, 'SANTIAGO DAVID', 'AQUINO GODOY', 5013645);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (311, ' ROCIO CELESTE', 'AGUAYO', 2217002);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (312, 'ANA MARGARITA', 'CHUANG PORTILLO', 1262791);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (313, 'BLAS HECTOR', 'BERNAL LEIVA', 3826380);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (314, 'CARLOS ISABELINO', 'GIMENEZ LEZCANO', 1831767);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (315, 'CARLOS JORGE', 'PARIS FERRARO', 694087);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (316, 'DIANA BEATRIZ', 'VERA MERCADO', 3752113);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (317, 'ERNESTO ANTONIO', 'PAREDES LEGUIZAMON', 2499479);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (318, 'FERNANDO AUGUSTO', 'VERON', 3533398);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (319, 'NORMA IRENE RUIZ DIAZ', NULL, 1208497);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (320, 'OSCAR DIAZ', NULL, 3179905);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (321, 'SANTIAGO GABRIEL ', 'MARECO AQUINO', 2500621);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (322, 'SARA LORENA', 'CACERES ACUÑA', 3721974);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (323, 'SERGIO ISMAEL', 'MARIN GONZALEZ', 2338615);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (324, 'VICTORINO', 'DENIS NUÑEZ', 653422);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (325, 'VIRGINIA', 'LOPEZ', 1102518);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (326, 'WILFRIDA RAMONA ', 'FIGUEREDO', 1212990);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (327, 'NIEVE EVA ', 'PONCE DE LEON', 819802);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (328, 'MARCOS ALFREDO', 'TORRES FERNANDEZ', 328200);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (329, 'OSCAR DARIO', 'VELAZQUEZ MEDINA', 1828649);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (330, 'CARLOS AUGUSTO', 'ZALDIVAR ACEVAL', 4223735);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (331, 'CARMEN ADELA ELIZABETH', 'YEGROS DE CARVALLO', 381181);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (332, 'GLORIA BALBINA', 'VILLALBA MIÑARRO', 302004);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (333, 'HUGO DANIEL', 'RIVEROS ROTELA', 1073023);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (334, 'GLADYS RAQUEL', 'PALACIOS GAONA', 3332514);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (335, 'ESTANISLAO', 'DUARTE ROTTONDO', 1110838);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (336, 'ABEL', 'JARA RAMIREZ', 701301);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (337, 'AGUSTIN', 'INSFRAN RIOS', 644964);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (338, 'AGUSTINA', 'MENDEZ', 1229121);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (339, 'LILA ALBA LETICIA', 'RAMOS DIAZ', 3467689);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (340, 'ROBERTO RAMON', 'MEAURIO LÓPEZ', 4247979);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (341, 'ALCIBIADES ANTONIO', 'SORIA SAMANIEGO', 902023);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (342, 'BRUNO DANIEL ', 'SOSA BAREIRO', 3768405);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (343, 'ANA MARIA', 'ARMOA CHAMORRO', 1472013);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (344, 'ANDREA CELESTE', 'FERNANDEZ NOGUERA', 3653195);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (345, 'ARISTIDES ', 'AGÜERO OLMEDO', 5447451);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (346, 'ALBERTO BALBUENA ZOILAN', NULL, 963834);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (347, 'LIDA STUMPFS', NULL, 706357);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (348, 'FELIX ROMERO', NULL, 3643824);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (349, 'LUISA DE JESUS DAVALOS', NULL, 3379753);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (350, 'CECILIA BRITEZ', NULL, 654386);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (351, 'OSCAR H. COLMAN G.', NULL, 1063538);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (352, 'BERTA AMALIA SEGOVIA WA', NULL, 1058382);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (353, 'LAURA CAROLINA VERA TOR', NULL, 5265745);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (354, 'RICARDO CABALLERO', NULL, 1193376);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (355, 'CARLOS ESPINOLA', NULL, 368056);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (356, 'EDISON JAVIER MOISES CE', NULL, 2510333);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (357, 'ROBERT ISMAEL AYALA', NULL, 3215576);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (358, 'JOSE E. RAMIREZ DELVALL', NULL, 2837774);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (359, 'CALIXTA MOURA', NULL, 483560);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (360, 'MIGUEL ANGEL COLMAN B.', NULL, 3525701);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (361, 'NANCY PEREZ', NULL, 654599);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (362, 'DANIEL A. PECCI', NULL, 1700565);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (363, 'CEMIA', 'MENDOZA VEGA', 2405777);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (364, 'HUGO MARCELO ', 'GONZALEZ BOGADO', 860350);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (365, 'JAVIER ANTONIO', 'VILLAMAYOR ESQUIVEL', 903633);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (366, 'LUIS JORGE', 'VILLALBA OCAMPOS', 696220);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (367, 'MARIA LUISA', 'VERA GOMEZ', 364624);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (368, 'MONICA', 'ENCINA', 1047147);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (369, 'OLGA YOLANDA', 'VILLALBA DE DAGUERRE', 1043774);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (370, 'OSCAR RAUL', 'VERA GAMARRA', 577857);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (371, 'PATRICIA ALEJANDRA', 'ZAYAS', 4169716);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (372, 'DIONISIO ', 'ACOSTA ALVARENGA', 3651280);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (373, 'ELVA MARIA', 'VILLALBA PEREIRA', 2012280);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (374, 'GUILLERMO ARNALDO ', 'ESCURRA VELAZQUEZ', 3195418);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (375, 'SANDRA IVANA', 'ZARACHO FRETES', 4565893);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (376, 'SARA LUISA', 'FLORENTIN DE AMARILLA', 1477349);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (377, 'SEBASTIAN', 'BENITEZ PALMA', 1855630);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (378, 'TEODORA CONCEPCIÓN', 'BRACHO PEDROZO', 1434554);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (379, 'ANGELA MONSERRAT', 'JARA OCAMPOS', 3527061);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (380, 'ADALBERTO JAVIER', 'BENITEZ GOMEZ', 1483635);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (381, 'ALBERT HERNAN', 'ARAUJO ZARZA', 1047804);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (382, 'NORMA ', 'LEGUIZAMON NUÑEZ', 1256851);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (383, 'MIRIAM DE JESUS', 'ARZAMENDIA DE BURGOS', 3527637);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (384, 'AVELINA', 'ARGAÑA', 1116654);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (385, 'BLANCA ZUNILDA', 'GARCIA DIAZ', 927307);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (386, 'CESAR FABIAN', 'ESPINOZA PAREDES', 3395444);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (387, 'MARIA AURORA', 'RUIZ DIAZ RUIZ DIAZ', 808911);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (388, 'MARIA TERESA ROLON', NULL, 916465);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (389, 'OSCAR R. BAREIRO N.', NULL, 1540420);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (390, 'CRISTINA DANIELA ', 'AMARILLA MARTINEZ', 2904263);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (391, 'HUGO NELSON', 'ACUÑA MILTOS', 1381586);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (392, 'ALBA MARIA ', 'BENITEZ CATTEBEKE', 3532903);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (393, 'PEDRO PABLO', 'PEREIRA GIMENEZ', 2387770);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (394, 'CARMELITA', 'GALLAGHER OVELAR', 1357381);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (395, 'CARMELO', 'VILLALBA', 1004443);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (396, 'CIRIA MABEL', 'ALTENHOFEN GERKE', 1558676);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (397, 'CIRIACO', 'OLMEDO', 1523396);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (398, 'DIEGO APOLONIO', 'OBELAR NUÑEZ', 2862473);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (399, 'ELIDA RAQUEL', 'CHAMORRO VALDEZ', 3215276);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (400, 'ESTELA', 'VALDEZ LOPEZ', 935664);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (401, 'EUGENIA', 'LOMBARDO', 1336225);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (402, 'RONALD LEANDRO', 'IRALA MEZA', 3500335);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (403, 'SANDRA LETICIA', 'LEZCANO SEGOVIA', 3588902);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (404, 'YAMIL ALFREDO', 'DAVALOS OLMEDO', 994751);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (405, 'EZEQUIEL CECILIO', 'DOMINGUEZ LLIRI', 751261);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (406, 'FRANCISCO RAMON', 'NUÑEZ AMARILLA', 1680992);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (407, 'JORGE  NICOLAS', 'FRETES DAVALOS', 1261765);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (408, 'FERNANDA ', 'ALCARAZ DE GONZALEZ', 4686487);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (409, 'NESTOR FABIAN', 'BARNI CENTURION', 2347727);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (410, 'NOELIA NATALIA ', 'SMITH RIVEROS', 3876329);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (411, 'NERY RAMON', 'CANTERO BAEZ', 3822831);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (412, 'ELPIDIO IVAN', 'CACERES', 4588597);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (413, 'NICOLAS RAFAEL', 'CAPORASO', 4228941);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (414, 'LAURA FELICIA', 'VILLALBA AGUILAR', 2472696);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (415, 'MAURICIO ANDRES', 'CACERES KRAUER', 3691971);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (416, 'OSCAR ARTURO', 'BARRETO SILVERA', 2240124);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (417, '1084278', NULL, 1084278);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (418, 'CESAR A. OCAMPOS S.', NULL, 3639696);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (419, 'ALDO MARTINEZ', NULL, 2442998);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (420, 'RODRIGO ROA FLORES', NULL, 3799420);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (421, 'MARIA CELSA AQUINO', NULL, 1537815);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (422, 'GLORIA DUARTE', NULL, 2406021);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (423, 'ELDA MARIA ', 'MONTIEL CENTURION', 4010792);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (424, 'LILIAN NOEMI', 'INSFRAN DE ACUÑA', 894680);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (425, 'ADRIANA ELIZABETH', 'CHAPARRO CARDOZO', 1782581);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (426, 'MARYAN ANDREA', 'COLMAN GONZALEZ', 3664292);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (427, 'MEDES JUSTO', 'MENDOZA VEGA', 821109);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (428, 'FERNANDO JHONI', 'BAEZ ACUÑA', 2545748);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (429, 'LAURA CLARIZZA', 'ACOSTA DE CAPDEVILA', 2442119);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (430, 'EDGAR CARMELO', 'CASADO CACERES', 2481721);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (431, 'NATHALIA MARIA', 'SILVA MIERES', 1490377);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (432, 'AIDA LUZ', 'ROMERO', 1031785);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (433, 'ANGEL DANIEL', 'MOREL DELGADO', 3935550);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (434, 'GILDA CATALINA', 'MARTINEZ DE CABRAL', 967005);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (435, 'IGNACIA RAMONA', 'MENDOZA DE GIMENEZ', 928591);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (436, 'JUAN IGNACIO', 'PAREDES SCHININI', 917645);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (437, 'LAURA ISABEL', 'LOPEZ DELGADO', 2482680);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (438, 'LEONARDO JAVIER', 'ORTIZ ORTIZ', 3182182);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (439, 'LILIAN GRISELDA', 'BENITEZ M.', 3405283);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (440, 'LIZA MARGARITA', 'FRANCO', 1432377);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (441, 'MARIO CESAR', 'VARGAS PERALTA', 3313703);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (442, 'ATILIO FERNANDEZ', NULL, 3528210);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (443, 'DOVAL TOMAS BENIGNO', 'BENITEZ VERA', 2201975);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (444, 'NATALIA CAROLINA', 'VOUGA OLMEDO', 2472704);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (445, '2432164', NULL, 2432164);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (446, 'JESUS MARIA', 'ROMERO RIVEROS', 3563619);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (447, 'SINFORIANO RAMON', 'AVALOS QUINTANA', 597838);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (448, 'RAMON RUBEN', 'OZUNA ', 4038846);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (449, 'LOURDES DE LAS NIEVES', 'CAÑETE DE OJEDA', 637960);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (450, 'HERCULANO', 'ISASI', 906654);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (451, 'REGINA', 'DOHMANN', 1601356);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (452, 'ISIDRO RAMON', 'SEGOVIA MOLINAS', 947677);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (453, 'CARLOS', 'AVEIRO NUÑEZ', 397237);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (454, 'ISABELINO', 'ACOSTA BENITEZ', 1421041);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (455, 'CRISTHIAN LUIS', 'GAMARRA MARECOS', 2179629);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (456, 'JOEL', 'CANO BOGARIN', 3784097);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (457, 'DAMIAN EULOGIO', 'AYALA LOPEZ', 389897);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (458, 'JOHANNA CAROLINA', 'MONTERO OLAIZOLA', 3615281);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (459, 'JORGE DANIEL', 'CAÑETE MERELES', 3395563);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (460, 'JOSE ALBERTO', 'MAIDANA PARRA', 1012923);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (461, 'MABEL ANTONIA', 'MARECOS RODRIGUEZ', 1488142);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (462, 'MARIA MERCEDES', 'ZORRILLA GIMENEZ', 623561);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (463, 'OSVALDO DANIEL', 'FLEITAS', 3194172);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (464, 'ENRIQUE FERRER', 'COLMAN MONZON', 901058);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (465, 'JESUS MARIA RAMON', 'VALENZUELA ARCE', 748530);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (466, 'BLAS ANTONIO', 'RIQUELME CARDOZO', 2497697);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (467, 'CESAR ANTONIO', 'MEDEN PELAEZ', 1255085);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (468, 'CARLOS ALBERTO', 'SANABRIA LANZONI', 1126856);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (469, 'SANDRA LORENA', 'CAPLI BASILI', 2896794);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (470, 'CARLOS ANTONIO', 'ALMADA PAREDES', 426413);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (471, 'CARMEN AMANDA', 'GUERREROS DE VELAZQUEZ', 543317);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (472, 'CATALINA BEATRIZ', 'ELIZECHE CENTURION', 612061);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (473, 'CESAR ENRIQUE', 'BENITEZ GOMEZ', 756893);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (474, 'CLARA', 'PANIAGUA', 3853972);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (475, 'EVARISTO RAMON', 'TORRES LEZCANO', 806890);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (476, 'AMILCAR MAXIMILIANO', 'CAZAL ECHAGUE', 1048660);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (477, 'GERARDO DANIEL', 'NEUMAN GOMEZ SANCHEZ', 4531236);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (478, 'GUILLERMO', 'RIVEROS MENDEZ', 1032068);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (479, 'JOSE LUIS', 'RODRIGUEZ TORNACO', 743951);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (480, 'JUAN ZENON', 'DOMINGUEZ', 975250);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (481, 'SUSANA ', 'BALBUENA DURE', 2058158);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (482, 'THALIA DESIREE', 'BENITEZ FARIA', 3650590);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (483, 'OSVALDO RODRIGO', 'GONZALEZ', 910587);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (484, 'SONIA PAOLA', 'ARAUJO MONGELOS', 1355159);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (485, 'SONNIA MARIA', 'CENTURION DE SERVIAN', 686326);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (486, 'ROSSANA RAQUEL', 'ESTIGARRIBIA AMARILLA', 1085622);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (487, 'NORMA BEATRIZ', 'ACOSTA INVERNIZZI', 1921037);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (488, 'ADRIANA GABRIELA', 'FERNANDEZ BENITEZ', 3640294);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (489, 'GUSTAVO ARRIOLA', NULL, 3197168);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (490, 'OLGA CARDOZO', NULL, 4371126);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (491, 'ADRIANA VILLALBA', NULL, 1884539);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (492, '4334400', NULL, 4334400);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (493, 'SILVIA', 'ARGUELLO ACUÑA', 1495683);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (494, 'MARIA JOSEFINA', 'SOLA RADICE', 436677);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (495, 'EDUARDO CALCENA', NULL, 733989);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (496, '4219820', NULL, 4219820);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (497, 'CELSO ALBERTO CARDENAS', NULL, 629188);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (498, 'MONICA ADRIANA', 'BOGARIN', 1547626);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (499, 'LUIS CONCEPCION', 'VALLEJOS AGUIRRE', 2210666);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (500, 'MARIA ADA', 'LEZCANO ROMAN', 791558);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (501, 'MARIA GILDA', 'ENCINA RODAS', 1705436);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (502, 'MIGUEL ANGEL ', 'RUIZ DIAZ ORTEGA', 800325);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (503, 'PEDRO JUAN', 'IGLESIAS MIERES', 3540029);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (504, 'ROBERTO', 'GONZALEZ ROMERO', 3213334);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (505, 'DAHIANA KARINA', 'URDAPILLETA SANABRIA', 4694917);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (506, 'FUNCIONARIO', 'DE PRUEBA', 2013165);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (507, 'ALICIA BEATRIZ', 'DUARTE ALUM', 1114121);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (508, 'GLORIA ELIZABETH', 'MOLAS TORALES', 1847982);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (509, 'ALIPIO ASUNCION', 'RIVAROLA AMARILLA', 1209234);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (510, 'CLAUDIA', 'DE GIMENEZ', 775152);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (511, 'CLELIA', 'BOGADO', 3208586);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (512, 'DIANA CAROLINA', 'CENTURION RAMIREZ', 1376433);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (513, 'FRANCISCA', 'OCAMPOS', 1924621);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (514, 'JOVINA', 'CANDIA BLANCO', 449365);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (515, 'MAXIMIANO', 'RAMIREZ', 484804);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (516, 'MARIA MAGDALENA', 'RAMIREZ AVALOS', 1139803);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (517, '3664300', NULL, 3664300);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (518, '3613491', NULL, 3613491);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (519, '1489602', NULL, 1489602);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (520, '4148237', NULL, 4148237);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (521, '4586262', NULL, 4586262);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (522, 'IVAN MATTO CANO', NULL, 3590469);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (523, 'MARIA ADORACION', 'BAZAN DE ROLON', 516750);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (524, 'CARLOS DARIO', 'INSFRAN SPELTT', 3775606);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (525, 'MIRTA ANTUNEZ', NULL, 405788);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (526, 'CARLOS ALBERTO ', 'SERVIN GUIRLAND', 1041951);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (527, 'JUAN VICENTE ', 'TALAVERA INSFRAN', 431842);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (528, 'CARLINO SAMUEL ', 'VELAZQUEZ MARTINEZ', 1077933);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (529, 'ROBERTO', 'GONZALEZ GONZALEZ', 2457941);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (530, 'ANTONIO GALO', 'CASTAGNINO GONZALEZ', 922494);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (531, 'ENRIQUE RAFAEL', 'BAEZ CANTERO', 984440);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (532, 'DIEGO GREGORIO', 'PEREIRA BENITEZ', 1432179);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (533, 'DOMINGA', 'ALVAREZ DE ARIZABAL', 564652);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (534, 'EDUARDO ANIBAL', 'DELGADO MACIEL', 715617);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (535, 'GLORIA', 'SANDOVAL PAREDES', 1541367);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (536, 'INGRID', 'GOIBURU OVIEDO', 3630897);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (537, 'ISABELINO', 'GODOY JARA', 377748);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (538, 'JESSICA LORENA', 'FERREIRA CARDOZO', 3792415);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (539, 'FATIMA MARIA ', 'FIGUEREDO BURGOS', 2966585);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (540, 'JENNY ', 'RUIZ DIAZ', 1227372);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (541, 'EVELIN ', 'ENCINA', 4171294);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (542, 'RODYS ', 'ROLON ALVARENGA', 1259777);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (543, 'ALCIDES RAMON ', 'VELAZQUEZ BOGADO', 2816105);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (544, 'JOAQUIN MIGUEL', 'LEZCANO', 2464159);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (545, 'JUAN RAMON', 'CABAÑAS PERALTA', 4392745);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (546, 'LIZ RAMONA', 'OZUNA CAÑETE', 6217191);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (547, 'JUAN SEBASTIAN', 'FERNANDEZ GIANGRECO', 4370153);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (548, '2488344', NULL, 2488344);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (549, 'CARLOS MISAEL FARIÑA', NULL, 3844689);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (550, 'PRUEBA', NULL, 12345);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (551, '3209870', NULL, 3209870);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (552, '764751', NULL, 764751);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (553, '5409889', NULL, 5409889);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (554, '4960567', NULL, 4960567);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (555, '4693095', NULL, 4693095);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (556, '803179', NULL, 803179);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (557, '1321456', NULL, 1321456);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (558, '1234456', NULL, 1234456);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (559, 'JOSE ANIBAL', 'GIMENEZ KULLAK', 1095180);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (560, 'MARCELO DANIEL', 'ROLON BAZAN', 3213270);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (561, 'MARCO ALFONSO', 'ROJAS VAZQUEZ', 1902610);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (562, 'MATEO', 'ALONSO HEURICH', 720367);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (563, 'RICHARD DARIO', 'CANTERO RAMOS', 2886348);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (564, 'RUBEN', 'CALDERON', 723531);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (565, 'WILSON ERICO', 'VILLALBA DUARTE', 3176299);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (566, 'MARIA CRISTINA', 'MONTIEL DE ESTIGARRIBIA', 869489);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (567, 'MARIA ISABEL', 'CORINA BRIZUELA', 692790);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (568, 'SILVIA MARLENE', 'GRAVO VARGAS', 3581400);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (569, 'VICTOR HUGO', 'NAVARRO MARIN', 553907);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (570, 'GRICELDA SOLEDAD', 'VARGAS', 2087599);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (571, 'VIRGINIA', 'ROJAS DE GARCETE', 2142908);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (572, 'ADRIANA BEATRIZ', 'DELGADO MARTINEZ', 1130363);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (573, 'ADRIANA BEATRIZ', 'GIMENEZ JIMENEZ', 3520995);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (574, 'ANIBAL FERNANDO', 'MENDIETA ZARZA', 992818);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (575, 'ANTONIA FREDESVINDA', 'ROJAS DE ENCISO', 566840);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (576, 'BLAS CANDELARIO', 'DELGADO MARTINEZ', 1130364);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (577, 'CESAR ESTEBAN', 'VELAZQUEZ ALONSO', 315026);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (578, 'CLAUDIA RAQUEL', 'DUARTE MIÑO', 3246654);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (579, 'EDITH CONCEPCION', 'ECHEVERRIA DE TRINIDAD', 580352);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (580, 'ELIAS', 'ROMAN', 450494);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (581, 'ELIGIO', 'GIMENEZ PEREIRA', 2123226);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (582, 'ERICO', 'VILLALBA DUARTE', 562733);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (583, 'TORIBIO EDILBERTO', 'ARGUELLO RIVAROLA', 689830);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (584, 'FABIOLA AIDEE', 'BURGOS ORUE', 2215775);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (585, 'FACUNDO', 'RODRIGUEZ LEIVA', 700716);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (586, 'FELICITA', 'VERA GONZALEZ', 1558216);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (587, 'FRANCISCO ADALBERTO', 'MENDEZ ROMERO', 951031);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (588, 'JUAN DAMIAN', 'AYALA RUIZ DIAZ', 2022486);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (589, 'JULIA', 'VARGAS DE GRAVO', 942513);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (590, 'CLAUDIA INES ', 'DINATALE WENNINGER', 3254696);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (591, 'DAVID', 'OZUNA RAJOY', 4345761);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (592, 'VICTOR FERNANDO', 'CAMACHO SANDOVAL', 1907366);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (593, 'AGUSTIN', 'VILLALBA', 830728);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (594, 'ALBA BELEN', 'MARTINEZ RAMIREZ', 3527916);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (595, 'MURIEL CELIA', 'BENDLIN CARNIBELLA', 3183745);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (596, 'ANTONIA TERESA', 'VERA DE SORIA', 458339);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (597, 'ARIEL RODRIGO', 'SOSA DIAZ', 2510260);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (598, 'BLANCA SARA', 'VELAZCO ROBALO', 426189);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (599, 'DIANA IRENE', 'VERA RICARDI', 3684880);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (600, 'DIOSNEL', 'SILVA RAMIREZ', 1552800);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (601, 'EDGAR RAUL', 'SUAREZ PEREIRA', 1771286);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (602, 'GABRIELA MARLENE', 'VILLAGRA MAIDANA', 4982652);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (603, 'VANIA ELIZABETH', 'ORTEGA RODRIGUEZ', 3669200);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (604, 'MARIA LUISA', 'ALMEIDA', 764715);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (605, 'MARIA CECILIA', 'LLERENA MASA', 2462553);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (606, 'VANESSA ', 'VERON CATEBEKE', 6616142);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (607, 'HORACIO ', 'TORREANI', 2241861);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (608, 'JENNIFER ', 'SNAIDER', 7026125);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (609, 'AMALIA ', 'ALMADA ALEGRE', 4347434);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (610, 'VICTOR', 'LUIZZI', 2507333);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (611, 'CARLOS ', 'OSORIO', 1982789);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (612, 'FELIX', 'TORRES ORTIGOZA', 347040);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (613, 'FLORA TERESA', 'SPERATTI', 618319);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (614, 'GLORIA ZUNILDA', 'ZARACHO DE CACERES', 618398);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (615, 'GUSTAVO JAVIER', 'SOVERINA GOROSITO', 501021);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (616, 'HUGO GERMAN', 'TALAVERA MENDIETA', 397275);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (617, 'JOSE AUGUSTO', 'VALIENTE PEREIRA', 3535833);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (618, 'LILIAN VERONICA', 'TROCHE', 3320230);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (619, 'MIGUEL ANTONIO', 'ZARATE YRALA', 4054780);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (620, 'VICTOR RAUL', 'CASTRO PAIVA', 1119767);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (621, 'SONIA ESPERANZA', 'CUELLAR', 762124);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (622, 'SONIA MARIA', 'SERVIN ORTIZ', 1253455);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (623, 'TERESA', 'JARA GOMEZ', 1435140);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (624, 'TERESA DANIELA', 'FINES', 1135161);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (625, 'TOMAS', 'SCHAPT LOPEZ', 2369218);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (626, 'ULISE ORLANDO', 'RUIZ DIAZ BRIZUELA', 1552865);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (627, 'VALENTINA', 'VERDEJO LOPEZ', 2174111);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (628, 'GLORIA MARÍA ', 'VILLAGRA CARRILLO', 1436936);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (629, 'VANESA MARIA', 'AGUILERA CAMPUZANO', 3451800);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (630, 'VANESSA GLORIA NADIR', 'ESCOBAR FRANCO', 2939026);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (631, 'VICTOR', 'MONGES ROMERO', 499793);
INSERT INTO paciente (id, nombres, apellidos, ci) VALUES (632, 'JOAN ANDRES', 'RUAX CABALLERO', 3552348);


--
-- TOC entry 2006 (class 0 OID 0)
-- Dependencies: 170
-- Name: paciente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('paciente_id_seq', 632, true);


--
-- TOC entry 1984 (class 0 OID 16456)
-- Dependencies: 175
-- Data for Name: profesional; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (1, 'Macaulay', 'Bates', '4000000', 'NO1109529726915');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (2, 'Olga', 'Gibson', '4000001', 'EE679719682904530406');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (3, 'Kitra', 'Callahan', '4000002', 'FO6456876079154714');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (4, 'Jana', 'Roth', '4000003', 'GB88OZSV87654478232690');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (5, 'Kelly', 'Ryan', '4000004', 'ES5932289868055982426101');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (6, 'Patricia', 'Gomez', '4000005', 'VG4743300782906183060097');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (7, 'Mira', 'Delacruz', '4000006', 'MD3369058095834316614069');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (8, 'Leslie', 'Terry', '4000007', 'LB51139790142289902851804199');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (9, 'Samantha', 'Carlson', '4000008', 'MR4986450886105720253034946');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (10, 'Yuri', 'Jones', '4000009', 'KZ528861190552609342');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (11, 'Elijah', 'Armstrong', '4000010', 'KZ393819427328970731');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (12, 'Bevis', 'Sharp', '4000011', 'LU070740454994929440');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (13, 'Ria', 'Rollins', '4000012', 'TN8414013032281123354948');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (14, 'MacKensie', 'Rollins', '4000013', 'SK0610118009624017079071');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (15, 'Xavier', 'Ayers', '4000014', 'LT576742542344091995');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (16, 'Garrett', 'Noel', '4000015', 'GE75198331966366107942');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (17, 'Rudyard', 'Bean', '4000016', 'LT529453494435629084');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (18, 'Nina', 'Gray', '4000017', 'AZ66759731200093553774772603');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (19, 'Adele', 'Reese', '4000018', 'GE12623699742880696468');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (20, 'Josiah', 'Thompson', '4000019', 'KW8265186582037317263969169481');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (21, 'Marcia', 'Haley', '4000020', 'LU230162782335170457');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (22, 'Wilma', 'Garcia', '4000021', 'BA778733420215757410');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (23, 'Deanna', 'Hale', '4000022', 'GB80URGE32941514150531');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (24, 'Colton', 'Bell', '4000023', 'GR8444305654016389719585660');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (25, 'Megan', 'Marquez', '4000024', 'ME60330831160097019108');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (26, 'Harriet', 'Finch', '4000025', 'DO96340117093803636508809639');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (27, 'Halla', 'Bond', '4000026', 'PS851636730485731167758623786');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (28, 'Gretchen', 'Soto', '4000027', 'BE11224309704749');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (29, 'Margaret', 'Cantu', '4000028', 'KZ585391955730965803');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (30, 'Hanae', 'Weiss', '4000029', 'AE556487053856364050185');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (31, 'Dacey', 'Smith', '4000030', 'MT64JEZY51803665740700296699853');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (32, 'Portia', 'Adams', '4000031', 'DE12855857414954364758');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (33, 'Lara', 'Gutierrez', '4000032', 'AD2908316114271355345937');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (34, 'Guy', 'Bridges', '4000033', 'FO9266779479525769');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (35, 'Kirestin', 'Gill', '4000034', 'BA286266153849516351');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (36, 'Abel', 'Walter', '4000035', 'AD6403873186964916790732');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (37, 'Jacqueline', 'Burks', '4000036', 'AE457765981053838051072');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (38, 'Glenna', 'Fischer', '4000037', 'VG5862558881314247598495');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (39, 'Clark', 'Chang', '4000038', 'GE57264544118529004423');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (40, 'Ora', 'Griffith', '4000039', 'SA7933534470357119696815');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (41, 'Nero', 'Holland', '4000040', 'MR7702890536479692709282561');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (42, 'Shaeleigh', 'Hayes', '4000041', 'CH5854298919300468959');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (43, 'Galena', 'Huff', '4000042', 'DE09218857459103808453');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (44, 'Malcolm', 'Lucas', '4000043', 'LI8729665987635148507');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (45, 'Lane', 'Rowe', '4000044', 'PL20491135454705073331022821');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (46, 'Damon', 'Logan', '4000045', 'GB25FGXT10914505667234');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (47, 'September', 'Christian', '4000046', 'MD0739511012361583055781');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (48, 'Sage', 'Whitney', '4000047', 'SE5076195403943821782243');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (49, 'Zelenia', 'Weaver', '4000048', 'FR3681068201772537740763880');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (50, 'Cassady', 'Beck', '4000049', 'NO3789734502561');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (51, 'Kamal', 'Clay', '4000050', 'GE45021844810846307114');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (52, 'Chandler', 'Ball', '4000051', 'BE93717476563371');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (53, 'Reese', 'Calderon', '4000052', 'CY41246923144842328574595175');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (54, 'Xantha', 'Weeks', '4000053', 'SA6104041922805189629609');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (55, 'Halla', 'Dunn', '4000054', 'CY85958019145988336625390719');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (56, 'Ian', 'Flowers', '4000055', 'CR9077668975660821386');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (57, 'Germane', 'Reilly', '4000056', 'NL47LFRC5162658622');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (58, 'Cassady', 'Wright', '4000057', 'GT62531839061117845018899192');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (59, 'Dale', 'Dillon', '4000058', 'PS239338117096386910239746275');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (60, 'Caryn', 'Torres', '4000059', 'CH6690526530049324187');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (61, 'Lucy', 'Moreno', '4000060', 'MC7839716147476477341252119');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (62, 'Aristotle', 'Oconnor', '4000061', 'SI93778852540870785');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (63, 'Octavius', 'Brewer', '4000062', 'IS371456111500905391198508');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (64, 'Benjamin', 'Kline', '4000063', 'CR0778725560786777440');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (65, 'Charles', 'Bradford', '4000064', 'LT530717877988947606');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (66, 'Rachel', 'Cantu', '4000065', 'GE75526223038681962910');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (67, 'Elmo', 'Everett', '4000066', 'AL87259580946130695074725388');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (68, 'Deacon', 'Compton', '4000067', 'AE628422866725481625521');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (69, 'Lacy', 'Wheeler', '4000068', 'MK90270747947806361');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (70, 'Jacqueline', 'Miller', '4000069', 'GB58BDMB29270898150714');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (71, 'Jordan', 'Wyatt', '4000070', 'ME72103637839610948493');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (72, 'Mona', 'Potter', '4000071', 'MC2256872312787602055797672');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (73, 'Gabriel', 'Jensen', '4000072', 'PK9013409943527624628505');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (74, 'Castor', 'Mckinney', '4000073', 'KZ143727216860218684');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (75, 'Desiree', 'Lindsey', '4000074', 'SE7380215967502953842567');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (76, 'Laurel', 'Farmer', '4000075', 'IT594JDGUI23913323009961765');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (77, 'Erich', 'Burke', '4000076', 'AD9467657692147543188390');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (78, 'Camilla', 'Bird', '4000077', 'GI52DQUS635035409724806');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (79, 'Belle', 'Barnes', '4000078', 'FI0389694854701282');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (80, 'Kirby', 'Todd', '4000079', 'DK8911913799191937');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (81, 'Isaiah', 'Pennington', '4000080', 'GT66525709452151133511937227');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (82, 'Wylie', 'Calhoun', '4000081', 'MT27GCCI44166684632129465691918');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (83, 'Colt', 'Spencer', '4000082', 'SK4813936699193191127953');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (84, 'Grady', 'Navarro', '4000083', 'CZ8279481968507286542766');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (85, 'Scarlett', 'Atkinson', '4000084', 'LB95359146774999364898044337');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (86, 'Hyatt', 'Johnston', '4000085', 'CZ9553830231326068387523');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (87, 'Dieter', 'Barrera', '4000086', 'AE139730856423524956129');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (88, 'Shafira', 'Johns', '4000087', 'BE33857988259426');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (89, 'Ray', 'Henson', '4000088', 'FO7477162099507990');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (90, 'Ira', 'Lucas', '4000089', 'DE23760479674806535375');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (91, 'Octavia', 'Haney', '4000090', 'BH71405071912533834652');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (92, 'Blythe', 'Mcgee', '4000091', 'SM4287931268861851105515435');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (93, 'Miriam', 'Benjamin', '4000092', 'CZ4518389697066482073502');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (94, 'Kylie', 'Bowers', '4000093', 'SA0918395349312351082398');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (95, 'Bruce', 'Simmons', '4000094', 'FR7758451605068168771948107');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (96, 'Caldwell', 'Navarro', '4000095', 'IS195565969503111250011996');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (97, 'Olivia', 'Mathis', '4000096', 'FO2996664796912634');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (98, 'Sharon', 'Olsen', '4000097', 'GT39705065298508312355828040');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (99, 'Ann', 'Dawson', '4000098', 'TN9193303850050546671581');
INSERT INTO profesional (id, nombres, apellidos, ci, nro_registro) VALUES (100, 'Barry', 'Gregory', '4000099', 'MC5925442623773063247186125');


--
-- TOC entry 2007 (class 0 OID 0)
-- Dependencies: 174
-- Name: profesional_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('profesional_id_seq', 100, true);


--
-- TOC entry 1988 (class 0 OID 16580)
-- Dependencies: 179
-- Data for Name: vacuna; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (1, 'ANTI-INFLUENZA ESTACIONAL', 'HU46466828142838777607838826', 'GlaxoSmithKline Biologicals', '2014-06-01 00:00:00', '2017-07-02 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (2, 'TD', 'TN1342974167948490914941', 'CSL Limited', '2014-09-28 00:00:00', '2017-07-13 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (3, 'DPT', 'KW4517219990554502960044014013', 'Merck & Co, Inc', '2014-11-14 00:00:00', '2019-10-21 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (4, 'OPV(SABIN ORAL)', 'HR8548550922316779112', 'Merck & Co, Inc', '2013-01-27 00:00:00', '2018-03-01 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (5, 'SPR', 'CH7384132704721629983', 'Sanofi Pasteur, SA', '2014-10-10 00:00:00', '2019-06-08 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (6, 'OPV(SABIN ORAL)', 'NL94UZMF7495139407', 'CSL Limited', '2012-11-30 00:00:00', '2018-11-13 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (7, 'BCG', 'SA7797964029650158492817', 'MedImmune LLC', '2015-02-01 00:00:00', '2019-11-27 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (8, 'OPV(SABIN ORAL)', 'FR4621730517749901628664662', 'CSL Limited', '2013-07-11 00:00:00', '2017-09-13 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (9, 'TD', 'MD3369787709172796510739', 'GlaxoSmithKline Biologicals', '2013-08-17 00:00:00', '2020-04-22 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (10, 'SPR', 'GI03XRAF686076334904691', 'GlaxoSmithKline Biologicals, S.A.', '2014-09-16 00:00:00', '2019-12-09 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (11, 'HB', 'VG1986150536373932836434', 'GlaxoSmithKline Biologicals, S.A.', '2015-01-24 00:00:00', '2019-11-19 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (12, 'BCG', 'FR3158115658457844485637647', 'CSL Limited', '2013-11-11 00:00:00', '2018-08-25 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (13, 'OPV(SABIN ORAL)', 'PK0796538760796467269522', 'MedImmune LLC', '2013-11-13 00:00:00', '2017-12-28 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (14, 'OPV(SABIN ORAL)', 'GR7927720878107301506294505', 'GlaxoSmithKline Biologicals, S.A.', '2012-12-21 00:00:00', '2019-07-06 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (15, 'ROTAVIRUS', 'KW1064234236211597068359833005', 'Sanofi Pasteur, SA', '2014-09-12 00:00:00', '2020-03-17 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (16, 'PENTAVALENTE (DPT+HB+HIB)', 'GE12102318783468615124', 'Merck & Co, Inc', '2013-03-07 00:00:00', '2018-09-26 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (17, 'ANTI-INFLUENZA ESTACIONAL', 'KW0689045763431990085985889304', 'CSL Limited', '2015-03-27 00:00:00', '2019-07-19 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (18, 'PENTAVALENTE (DPT+HB+HIB)', 'AZ43111339312128160519793979', 'MedImmune LLC', '2012-12-26 00:00:00', '2019-10-27 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (19, 'SPR', 'MC0540152509994742556567352', 'GlaxoSmithKline Biologicals', '2013-07-01 00:00:00', '2020-03-13 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (20, 'BCG', 'LT310624856759438465', 'GlaxoSmithKline Biologicals, S.A.', '2014-10-09 00:00:00', '2018-12-28 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (21, 'OPV(SABIN ORAL)', 'BE38047212172028', 'Sanofi Pasteur, SA', '2013-06-06 00:00:00', '2018-03-25 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (22, 'HB', 'GE78696984782405260179', 'CSL Limited', '2014-08-15 00:00:00', '2017-07-27 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (23, 'OPV(SABIN ORAL)', 'DE29732366696429345774', 'CSL Limited', '2012-06-09 00:00:00', '2019-01-09 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (24, 'AA', 'SM5202699130062048043781438', 'GlaxoSmithKline Biologicals', '2014-06-15 00:00:00', '2019-01-03 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (25, 'DPT', 'PT20837324802167249739727', 'Merck & Co, Inc', '2013-02-23 00:00:00', '2019-08-10 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (26, 'HB', 'MR4673838957623843077438206', 'Sanofi Pasteur, SA', '2013-05-10 00:00:00', '2019-12-01 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (27, 'BCG', 'ES0578177798345231345518', 'GlaxoSmithKline Biologicals, S.A.', '2014-05-09 00:00:00', '2017-10-20 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (28, 'SPR', 'BH91723603921475324483', 'Sanofi Pasteur, SA', '2014-05-19 00:00:00', '2018-11-10 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (29, 'DPT', 'FR8884683830962278403986043', 'GlaxoSmithKline Biologicals', '2013-04-11 00:00:00', '2018-12-23 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (30, 'OPV(SABIN ORAL)', 'RS61810789285947627417', 'GlaxoSmithKline Biologicals, S.A.', '2013-05-25 00:00:00', '2018-12-17 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (31, 'OPV(SABIN ORAL)', 'MD9281500412543698400209', 'MedImmune LLC', '2013-11-02 00:00:00', '2017-06-21 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (32, 'TD', 'SM8074369467240747205544616', 'Barr Labs, Inc', '2012-09-19 00:00:00', '2018-03-20 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (33, 'PENTAVALENTE (DPT+HB+HIB)', 'BA262174712614226721', 'GlaxoSmithKline Biologicals', '2012-09-15 00:00:00', '2018-07-13 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (34, 'AA', 'SI56352911605481784', 'MedImmune LLC', '2015-02-02 00:00:00', '2019-12-17 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (35, 'BCG', 'ES2748083568985685953311', 'CSL Limited', '2013-07-14 00:00:00', '2019-06-21 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (36, 'AA', 'IL533924944336676892851', 'GlaxoSmithKline Biologicals, S.A.', '2013-02-16 00:00:00', '2018-05-23 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (37, 'DPT', 'AZ09888386220964753648301573', 'Barr Labs, Inc', '2015-02-08 00:00:00', '2019-03-08 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (38, 'PENTAVALENTE (DPT+HB+HIB)', 'SM2685048220946283201251694', 'Sanofi Pasteur, SA', '2015-04-02 00:00:00', '2018-10-10 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (39, 'ANTI-INFLUENZA ESTACIONAL', 'SA0634780060505895612810', 'Sanofi Pasteur, SA', '2015-03-07 00:00:00', '2018-11-07 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (40, 'SPR', 'FR6283697524994622046223988', 'GlaxoSmithKline Biologicals', '2012-12-18 00:00:00', '2017-10-11 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (41, 'TD', 'BG91HUQW40121893105827', 'GlaxoSmithKline Biologicals', '2013-07-05 00:00:00', '2018-11-16 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (42, 'OPV(SABIN ORAL)', 'PS915309962312802200489199103', 'MedImmune LLC', '2012-07-22 00:00:00', '2020-05-02 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (43, 'DPT', 'LV34MNQB5446304179973', 'Merck & Co, Inc', '2014-09-15 00:00:00', '2019-02-19 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (44, 'PENTAVALENTE (DPT+HB+HIB)', 'IL516076846631913473342', 'Sanofi Pasteur, SA', '2012-10-17 00:00:00', '2018-05-28 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (45, 'HB', 'PK6259881767575825279496', 'Barr Labs, Inc', '2015-03-20 00:00:00', '2020-05-23 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (46, 'BCG', 'LV05FMMB3124592678028', 'Barr Labs, Inc', '2013-06-01 00:00:00', '2018-08-05 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (47, 'DPT', 'CY98708097034432963970053911', 'Sanofi Pasteur, SA', '2014-06-13 00:00:00', '2017-12-24 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (48, 'ROTAVIRUS', 'LB55588123845169592515630286', 'Merck & Co, Inc', '2015-02-06 00:00:00', '2018-05-12 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (49, 'SPR', 'GB67KFRH20897245170005', 'Barr Labs, Inc', '2014-12-10 00:00:00', '2018-08-29 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (50, 'BCG', 'FO8043983789745793', 'GlaxoSmithKline Biologicals, S.A.', '2013-01-17 00:00:00', '2018-03-25 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (51, 'ROTAVIRUS', 'MT28YBLO65746471411889185009470', 'GlaxoSmithKline Biologicals, S.A.', '2014-08-24 00:00:00', '2018-02-20 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (52, 'TD', 'IE10DCGR24510615552840', 'Barr Labs, Inc', '2014-09-28 00:00:00', '2018-04-04 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (53, 'ROTAVIRUS', 'BE84451454613135', 'GlaxoSmithKline Biologicals, S.A.', '2013-04-25 00:00:00', '2018-07-11 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (54, 'BCG', 'NL41AFJL4035691255', 'GlaxoSmithKline Biologicals, S.A.', '2013-03-27 00:00:00', '2019-12-12 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (55, 'ANTI-INFLUENZA ESTACIONAL', 'NL12XRFT7606575937', 'CSL Limited', '2014-10-27 00:00:00', '2018-10-26 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (56, 'SPR', 'RO47LXUG1379195613020002', 'MedImmune LLC', '2013-04-08 00:00:00', '2017-10-09 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (57, 'ANTI-INFLUENZA ESTACIONAL', 'FO8790780038833449', 'GlaxoSmithKline Biologicals', '2013-05-31 00:00:00', '2020-05-17 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (58, 'DPT', 'BE89510460174668', 'CSL Limited', '2013-05-03 00:00:00', '2019-04-27 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (59, 'HB', 'LT423772512796854552', 'CSL Limited', '2013-07-25 00:00:00', '2017-08-16 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (60, 'ROTAVIRUS', 'LI5948917035658069818', 'Sanofi Pasteur, SA', '2013-07-26 00:00:00', '2017-11-01 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (61, 'ROTAVIRUS', 'LT142139931952302589', 'CSL Limited', '2012-07-28 00:00:00', '2019-02-05 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (62, 'SPR', 'BE23488795316357', 'MedImmune LLC', '2013-08-23 00:00:00', '2018-02-04 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (63, 'HB', 'HR9870767938306170699', 'CSL Limited', '2014-02-04 00:00:00', '2020-04-15 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (64, 'HB', 'SE0931545456515487352164', 'MedImmune LLC', '2013-01-19 00:00:00', '2017-10-01 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (65, 'SPR', 'BH57546729723979595461', 'Sanofi Pasteur, SA', '2014-03-14 00:00:00', '2018-06-06 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (66, 'PENTAVALENTE (DPT+HB+HIB)', 'IT287RJYFI42553861736932842', 'Sanofi Pasteur, SA', '2013-09-08 00:00:00', '2020-02-17 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (67, 'DPT', 'FR3729425772382852553228573', 'CSL Limited', '2013-02-08 00:00:00', '2019-09-18 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (68, 'OPV(SABIN ORAL)', 'CY25664738609121482833618726', 'GlaxoSmithKline Biologicals', '2014-11-12 00:00:00', '2019-02-06 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (69, 'OPV(SABIN ORAL)', 'AT912650831598109003', 'CSL Limited', '2015-04-25 00:00:00', '2018-12-02 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (70, 'BCG', 'SK6236906094252977936701', 'GlaxoSmithKline Biologicals', '2012-07-22 00:00:00', '2017-09-22 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (71, 'HB', 'KW5502054531016069772795908962', 'GlaxoSmithKline Biologicals, S.A.', '2014-07-09 00:00:00', '2019-11-14 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (72, 'SPR', 'TR226496819398341090606804', 'Barr Labs, Inc', '2014-03-16 00:00:00', '2020-02-23 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (73, 'AA', 'AE813650572344676098239', 'Sanofi Pasteur, SA', '2013-07-22 00:00:00', '2019-01-06 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (74, 'PENTAVALENTE (DPT+HB+HIB)', 'BH73772978693767350474', 'GlaxoSmithKline Biologicals', '2014-08-31 00:00:00', '2020-04-23 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (75, 'OPV(SABIN ORAL)', 'AE875135602960281714823', 'GlaxoSmithKline Biologicals', '2013-10-27 00:00:00', '2018-08-26 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (76, 'OPV(SABIN ORAL)', 'BG71AAXK88019082811615', 'MedImmune LLC', '2015-01-25 00:00:00', '2018-07-02 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (77, 'ROTAVIRUS', 'NO7302734839521', 'CSL Limited', '2014-12-24 00:00:00', '2017-08-18 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (78, 'BCG', 'ES7718589964995138389269', 'GlaxoSmithKline Biologicals, S.A.', '2015-01-31 00:00:00', '2020-03-22 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (79, 'PENTAVALENTE (DPT+HB+HIB)', 'GI96RJTP018251379879925', 'Barr Labs, Inc', '2013-01-22 00:00:00', '2020-03-20 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (80, 'OPV(SABIN ORAL)', 'ME60039362962969608278', 'Merck & Co, Inc', '2013-02-26 00:00:00', '2019-07-19 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (81, 'ANTI-INFLUENZA ESTACIONAL', 'MD6225919659365910691658', 'GlaxoSmithKline Biologicals', '2012-10-18 00:00:00', '2020-03-19 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (82, 'PENTAVALENTE (DPT+HB+HIB)', 'IL229864241145596425667', 'Sanofi Pasteur, SA', '2013-03-23 00:00:00', '2017-06-02 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (83, 'SPR', 'GE73457133441796349822', 'Sanofi Pasteur, SA', '2012-07-09 00:00:00', '2019-11-02 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (84, 'OPV(SABIN ORAL)', 'GE80467054209601397292', 'GlaxoSmithKline Biologicals, S.A.', '2013-11-06 00:00:00', '2018-06-23 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (85, 'TD', 'LT371042380600827389', 'GlaxoSmithKline Biologicals', '2014-09-25 00:00:00', '2018-03-06 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (86, 'DPT', 'CH4917557054182713629', 'Sanofi Pasteur, SA', '2012-06-14 00:00:00', '2017-12-21 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (87, 'SPR', 'LI1836033696752322890', 'Sanofi Pasteur, SA', '2014-09-10 00:00:00', '2018-04-17 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (88, 'DPT', 'RS48144864681284888780', 'Merck & Co, Inc', '2014-09-15 00:00:00', '2018-11-07 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (89, 'AA', 'AZ19105161299610355980370558', 'GlaxoSmithKline Biologicals', '2015-03-19 00:00:00', '2017-12-13 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (90, 'TD', 'GI79ZORE123816893526527', 'GlaxoSmithKline Biologicals', '2015-03-19 00:00:00', '2019-04-21 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (91, 'HB', 'PT09507036582205399232394', 'GlaxoSmithKline Biologicals', '2013-12-27 00:00:00', '2020-05-19 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (92, 'AA', 'PK9262200564321941468742', 'Merck & Co, Inc', '2014-03-20 00:00:00', '2017-09-14 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (93, 'TD', 'RO03THYH5180680001325058', 'CSL Limited', '2013-05-23 00:00:00', '2018-04-13 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (94, 'BCG', 'LB94103878192600023135565589', 'MedImmune LLC', '2013-12-07 00:00:00', '2019-03-18 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (95, 'HB', 'SI52724069115350205', 'Merck & Co, Inc', '2013-02-19 00:00:00', '2018-09-15 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (96, 'ROTAVIRUS', 'MD9787247298609712019646', 'GlaxoSmithKline Biologicals', '2013-02-25 00:00:00', '2018-08-12 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (97, 'TD', 'BA973163836151772583', 'GlaxoSmithKline Biologicals', '2014-11-22 00:00:00', '2020-02-20 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (98, 'AA', 'GE06397803123586860316', 'CSL Limited', '2014-11-19 00:00:00', '2018-09-03 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (99, 'PENTAVALENTE (DPT+HB+HIB)', 'KW7585362196659379485245038692', 'MedImmune LLC', '2012-09-12 00:00:00', '2017-06-14 00:00:00', false);
INSERT INTO vacuna (id, nombre_vacuna, lote, laboratorio_denominacion, fecha_adquisicion, fecha_vencimiento, eliminado) VALUES (100, 'DPT', 'BA409105155382344046', 'Sanofi Pasteur, SA', '2013-06-13 00:00:00', '2019-01-02 00:00:00', false);


--
-- TOC entry 2008 (class 0 OID 0)
-- Dependencies: 178
-- Name: vacuna_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vacuna_id_seq', 100, true);


--
-- TOC entry 1865 (class 2606 OID 16552)
-- Name: administracion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY administracion
    ADD CONSTRAINT administracion_pkey PRIMARY KEY (id);


--
-- TOC entry 1859 (class 2606 OID 16412)
-- Name: paciente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY paciente
    ADD CONSTRAINT paciente_pkey PRIMARY KEY (id);


--
-- TOC entry 1861 (class 2606 OID 16424)
-- Name: producto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY centro_asistencial
    ADD CONSTRAINT producto_pkey PRIMARY KEY (id);


--
-- TOC entry 1863 (class 2606 OID 16461)
-- Name: profesional_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY profesional
    ADD CONSTRAINT profesional_pkey PRIMARY KEY (id);


--
-- TOC entry 1867 (class 2606 OID 16586)
-- Name: vacuna_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY vacuna
    ADD CONSTRAINT vacuna_pkey PRIMARY KEY (id);


--
-- TOC entry 1868 (class 2606 OID 16553)
-- Name: administracion_centro_asistencial_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administracion
    ADD CONSTRAINT administracion_centro_asistencial_fk FOREIGN KEY (id_centro_asistencial) REFERENCES centro_asistencial(id);


--
-- TOC entry 1870 (class 2606 OID 16595)
-- Name: administracion_paciente_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administracion
    ADD CONSTRAINT administracion_paciente_fk FOREIGN KEY (id_paciente) REFERENCES paciente(id);


--
-- TOC entry 1869 (class 2606 OID 16558)
-- Name: administracion_profesional_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administracion
    ADD CONSTRAINT administracion_profesional_fk FOREIGN KEY (id_profesional) REFERENCES profesional(id);


--
-- TOC entry 1871 (class 2606 OID 16600)
-- Name: administracion_vacuna_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administracion
    ADD CONSTRAINT administracion_vacuna_fk FOREIGN KEY (id_vacuna) REFERENCES vacuna(id);


--
-- TOC entry 1996 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-07-05 11:18:55

--
-- PostgreSQL database dump complete
--

