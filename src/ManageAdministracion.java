import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Example;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v231.group.VXU_V04_ORCRXARXROBXNTE;
import ca.uhn.hl7v2.model.v231.message.VXU_V04;
import ca.uhn.hl7v2.parser.CanonicalModelClassFactory;
import clienteRest.JerseyClient;
import entities.Administracion;
import entities.CentroAsistencial;
import entities.Paciente;
import entities.Profesional;
import entities.Vacuna;

public class ManageAdministracion {

	private static SessionFactory factory;
	private static ServiceRegistry serviceRegistry;

	private static ManageAdministracion me;

	public static void main(String[] args) {
		try {
			Configuration configuration = new Configuration();
			configuration.configure();
			serviceRegistry = new ServiceRegistryBuilder().applySettings(
					configuration.getProperties()).buildServiceRegistry();
			factory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			System.err.println("Fallo al crear sesion" + ex);
			throw new ExceptionInInitializerError(ex);
		}

		me = new ManageAdministracion();

		// UserFunctions uf = new UserFunctions();
		// me.postAdministracionAllPacientes();

		String respuestaHL7 = me.getAdministracionPacienteExterno("3563619");

		Message hapiMsg = me.parseHL7Message(respuestaHL7);

		// aca ya se obtiene el mensaje VXU
		VXU_V04 vxu = (VXU_V04) hapiMsg;

		me.guardarVXU(vxu);

		try {
			System.out.println("##########################");
			System.out.println(vxu.encode());
		} catch (HL7Exception e) {
			e.printStackTrace();
		}

	}

	public Message parseHL7Message(String stringMsg) {
		HapiContext context = new DefaultHapiContext();

		context.setModelClassFactory(new CanonicalModelClassFactory(
				VXU_V04.class));
		Message hapiMsg = null;
		try {
			hapiMsg = context.getPipeParser().parse(stringMsg);
			// context.close();
		} catch (HL7Exception e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hapiMsg;
	}

	public void postAdministracionAllPacientes() {
		/* Lista todos los pacientes de la base datos */
		List<Paciente> listaPacientes = me.listPacientes();

		List<String> listaPac = me
				.pacientesAdministracionToHL7v2(listaPacientes);

		System.out.println(listaPac.toString());
		JerseyClient.HTTPPost(listaPac.toString());
	}

	public String getAdministracionPacienteExterno(String cedula) {
		/* Lista todos los pacientes de la base datos */

		HashMap<String, String> hashParametros = new HashMap<String, String>();

		hashParametros.put("id", cedula);
		String msgHL7 = JerseyClient.HTTPget(hashParametros);

		return msgHL7;

	}

	public void guardarVXU(VXU_V04 vxu) {

		Paciente pac=null;
		String idString = null;
		idString = vxu.getPID().getPatientID().getCx1_ID().getValue();
		try {
			if (idString == null) {
				idString = "123456789";
				Paciente pacie=new Paciente();
				pacie.setCi(Integer.parseInt(idString));
				pac=me.getPacienteByExample(pacie);
				if(pac==null){
					pac=new Paciente();
					pac.setNombres(vxu.getPID().getPatientName(0).getGivenName().getValue());

					pac.setApellidos(vxu.getPID().getPatientName(0).getFamilyLastName()
							.getFamilyName().getValue());
				}
			}
			Integer idInteger = Integer.parseInt(idString);
		} catch (NumberFormatException nfe) {
			nfe.printStackTrace();
			System.out.println("No existe el id");
		}
		

		Integer counter = vxu.getORCRXARXROBXNTEReps();

		for (int i = 0; i < counter; i++) {
			Administracion administracionExample = new Administracion();

			// se llenan los datos de la adminitracion
			VXU_V04_ORCRXARXROBXNTE vxuV04 = vxu.getORCRXARXROBXNTE(i);

			Profesional profesional;

			// si no existe el profesional, se agrega a la base de datos. Si ya
			// existe, se utiliza el existente.
			String profesionalId = vxuV04.getRXA()
					.getRxa10_AdministeringProvider(0).getIDNumber().getValue();

			if (profesionalId == null) {
				profesionalId = "123";
			}
			Profesional pABuscar=new Profesional();
			pABuscar.setCi(profesionalId);
			profesional = me.getProfesionalByExample(pABuscar); // se busca en la bd

			if (profesional == null) { // no existe en la bd, entonces se
										// agregar el profesional
				profesional = new Profesional();
//				profesional.setId(Integer.parseInt(profesionalId));
				profesional.setCi(profesionalId);
				profesional.setNombres(vxuV04.getRXA()
						.getRxa10_AdministeringProvider(0).getGivenName()
						.getValue());
				profesional.setApellidos(vxuV04.getRXA()
						.getRxa10_AdministeringProvider(0).getFamilyLastName()
						.getFamilyName().getValue());
			}
			Vacuna vacExample = new Vacuna();
			vacExample.setNombreVacuna(vxuV04.getRXA()
					.getRxa5_AdministeredCode().getCe2_Text().getValue());

			List<Vacuna> listaVac = me.getVacunaByExample(vacExample);

			Vacuna vac = null;
			if (listaVac != null && listaVac.size() != 0) {
				vac = listaVac.get(0); // se trae la primera vacuna que se
										// encuentra.
			} else {
				vac = vacExample; // hay que guardar en bd si no existe
				vac.setNombreVacuna(vxuV04.getRXA().getRxa5_AdministeredCode()
						.getCe2_Text().getValue());

				vac.setLote(vxuV04.getRXA().getRxa15_SubstanceLotNumber(0)
						.getValue());
				vac.setLaboratorioDenominacion(vxuV04.getRXA()
						.getRxa17_SubstanceManufacturerName(0).getCe2_Text()
						.getValue());
				vac.setEliminado(false);

				try {
					Date fechaExp=vxuV04.getRXA()
							.getRxa16_SubstanceExpirationDate(0)
							.getTs1_TimeOfAnEvent().getValueAsDate();
							if(fechaExp!=null){
								vac.setFechaVencimiento(new Timestamp(fechaExp.getTime()));
							}
				} catch (DataTypeException e) {
					System.out.print("Error al convertir date en vacuna");
					e.printStackTrace();
				}

			}

			CentroAsistencial centroAsistExample = new CentroAsistencial();
			centroAsistExample.setNombre(vxuV04.getRXA()
					.getRxa11_AdministeredAtLocation().getFacility()
					.getNamespaceID().getValue());

			List<CentroAsistencial> listaCentroAsistencial = me
					.getCentroAsistencialByExample(centroAsistExample);

			CentroAsistencial centroAsistencial = null;
			if (listaCentroAsistencial != null
					&& listaCentroAsistencial.size() != 0) {
				centroAsistencial = listaCentroAsistencial.get(0); // se trae el
																	// primer
																	// centro
																	// asistencial
																	// que se
				// encuentra.
			} else {
				centroAsistencial = centroAsistExample; // hay que guardar en bd
														// si no existe
				centroAsistencial.setDireccion(vxuV04.getRXA()
						.getRxa11_AdministeredAtLocation().getStreetAddress()
						.getValue());
				centroAsistencial.setEliminado(false);
			}

			administracionExample.setCantidadAdministracion(new BigDecimal(
					vxuV04.getRXA().getRxa6_AdministeredAmount().getValue()));

			if (vxuV04.getRXA().getRxa9_AdministrationNotesReps() > 0) {
				administracionExample.setDescripcionObservacion(vxuV04.getRXA()
						.getRxa9_AdministrationNotes(0).getCe2_Text()
						.getValue());
			}

			try {
				administracionExample
						.setFechaAdministracion(new Timestamp(vxuV04.getRXA()
								.getRxa3_DateTimeStartOfAdministration()
								.getTimeOfAnEvent().getValueAsDate().getTime()));
			} catch (DataTypeException e) {
				e.printStackTrace();
			}
			administracionExample.setEliminado(false);
			administracionExample.setParteCuerpo(vxuV04.getRXR().getRxr2_Site()
					.getCe2_Text().getValue());
			administracionExample.setUnidades(vxuV04.getRXA()
					.getRxa7_AdministeredUnits().getCe2_Text().getValue());
			administracionExample.setVia(vxuV04.getRXR().getRxr1_Route()
					.getCe2_Text().getValue());

			// se debe enviar las entidades correspondientes, ya sean existentes
			// o nuevas
			administracionExample.setCentroAsistencial(centroAsistencial);
			administracionExample.setPaciente(pac);
			administracionExample.setProfesional(profesional);
			administracionExample.setVacuna(vac);

			Session session = (Session) factory.openSession();

			Example administracionExampleCriteria = Example
					.create(administracionExample);
			Example pacienteExample = Example.create(pac);
			Example profesionalExample = Example.create(profesional);
			Example vacunaExample = Example.create(vac);
			Criteria criteria = session.createCriteria(Administracion.class)
					.add(administracionExampleCriteria);
			criteria.createCriteria("paciente").add(pacienteExample);
			criteria.createCriteria("profesional").add(profesionalExample);
			criteria.createCriteria("vacuna").add(vacunaExample);

			// se lista las administracion si ya existe
			List<Administracion> listaAdmin = criteria.list();

			if (listaAdmin.size() == 0) {
				// se debe guardar
				System.out.println("Storing in the DB.. =D");
				
				Integer idAd=me.addAdministracion(administracionExample, centroAsistencial,pac, profesional, vac);
				
				System.out.println("Administracion guardada con ID="+idAd);
				
			} else {
				// ya existe
			}

		}

	}

	/* Method to CREATE an administracion in the database */
	public Integer addAdministracion(String cantidadAdministrada,
			String descripcion, String fechaAdministracion, String parteCuerpo,
			String unidades, String via, CentroAsistencial centroAsistencial,
			Paciente paciente, Profesional profesional, Vacuna vacuna) {

		Session session = factory.openSession();
		Transaction tx = null;
		Integer administracionID = null;
		try {
			tx = session.beginTransaction();
			Administracion administracion = new Administracion();
			administracion.setCantidadAdministracion(new BigDecimal(
					cantidadAdministrada));
			administracion.setDescripcionObservacion(descripcion);
			administracion.setFechaAdministracion(new java.sql.Timestamp(
					new SimpleDateFormat("dd-MM-yyyy").parse(
							fechaAdministracion).getTime()));
			administracion.setEliminado(false);
			administracion.setParteCuerpo(parteCuerpo);
			administracion.setUnidades(unidades);
			administracion.setVia(via);

			// se debe enviar las entidades correspondientes, ya sean existentes
			// o nuevas
			administracion.setCentroAsistencial(centroAsistencial);
			administracion.setPaciente(paciente);
			administracion.setProfesional(profesional);
			administracion.setVacuna(vacuna);

			administracionID = (Integer) session.save(administracion);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} catch (ParseException pe) {
			System.out.println("Error parsing date in fechaAdminitracion");
			pe.printStackTrace();
		} finally {
			session.close();
		}
		return administracionID;
	}

	/* Method to CREATE an administracion in the database */
	public Integer addAdministracion(Administracion administracion,
			CentroAsistencial centroAsistencial, Paciente paciente,
			Profesional profesional, Vacuna vacuna) {

		Session session = factory.openSession();
		Transaction tx = null;
		Integer administracionID = null;
		try {
			tx = session.beginTransaction();
			// se debe enviar las entidades correspondientes, ya sean existentes
			// o nuevas
			session.saveOrUpdate(centroAsistencial);
			session.saveOrUpdate(paciente);
			session.saveOrUpdate(profesional);
			session.saveOrUpdate(vacuna);
			
			administracion.setCentroAsistencial(centroAsistencial);
			administracion.setPaciente(paciente);
			administracion.setProfesional(profesional);
			administracion.setVacuna(vacuna);

			administracionID = (Integer) session.save(administracion);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return administracionID;
	}
	
	/* Method to CREATE an administracion in the database */
	public Integer addAdministracion(Administracion administracion) {

		Session session = factory.openSession();
		Transaction tx = null;
		Integer administracionID = null;
		try {
			tx = session.beginTransaction();
			// se debe enviar las entidades correspondientes, ya sean existentes
			// o nuevas
			administracionID = (Integer) session.save(administracion);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return administracionID;
	}

	/*
	 * Metodo que convierte las administraciones de cada paciente a una lista de
	 * string endoded de HL7v231
	 */
	public List<String> pacientesAdministracionToHL7v2(
			List<Paciente> listaPacientes) {
		String identifier;
		String date;// RXA-3-date/time start of administration
		String vaccineType;// RXA-5-administered code
		String subject;// PID-3-patient ID list
		String reported;// RXA-9
		String performerId, performerName, performerLastName;// RXA-10-administering
																// provider
		String requesterId, requesterName, requesterLastName;
		String requester;// ORC-12-ordering provider
		String manufacturer;// RXA-17-substance manufacturer name
		String location;// RXA-27-administered-at (or
						// RXA-11-administered-at
						// location, deprecated as of v2.7)
		String locationAddress;
		String lotNumber;// RXA-15-substance lot number
		String expirationDate;// RXA-16-substance expiration date
		String adminSiteId, adminSiteDescripcion;// RXR-2-administration
													// site
		String route;// RXR-1-route
		String doseQuantity; // RXA-6-administered amount /
								// RXA-7.1-administered
		String doseUnidadMedida; // units.code
		String refusalReasonId;// RXA-18-2
		String refusalReasonDescripcion;
		String refusalReasonCodDescrip;
		String reaction; // OBX-3
		String reactionDate;// OBX-14 (ideally this would be reported in
							// an IAM
							// segment, but IAM is not part of the V2.x
							// VXU
							// message - most likely would appear in OBX
							// segments if at all
		ArrayList<String> listaReturn = new ArrayList<String>();
		for (Paciente pa : listaPacientes) {
			List<Administracion> listaAd = pa.getAdministracions();

			HapiContext context = new DefaultHapiContext();
			context.setModelClassFactory(new CanonicalModelClassFactory(
					VXU_V04.class));

			subject = pa.getId() + "";
			VXU_V04 vxu = new VXU_V04();
			try {
				vxu.initQuickstart("VXU", "V04", "P");
				vxu.getPID().getPatientID().getCx1_ID().setValue(subject);
				vxu.getPID().getPatientName(0).getGivenName()
						.setValue(pa.getNombres());
				vxu.getPID().getPatientName(0).getFamilyLastName()
						.getFamilyName().setValue(pa.getApellidos());
			} catch (Exception e1) {
				System.out.println("Excepci�n en PID");
				e1.printStackTrace();
			}
			Integer counter = 0;
			for (Administracion ad : listaAd) {

				date = new SimpleDateFormat("yyyy-MM-dd").format(ad
						.getFechaAdministracion());

				subject = ad.getPaciente().getId() + "";
				reported = null;
				performerId = ad.getProfesional().getNroRegistro();
				performerName = ad.getProfesional().getNombres();
				performerLastName = ad.getProfesional().getApellidos();

				location = ad.getCentroAsistencial().getNombre();
				locationAddress = ad.getCentroAsistencial().getCiudad();

				vaccineType = ad.getVacuna().getNombreVacuna();
				lotNumber = ad.getVacuna().getLote();
				manufacturer = ad.getVacuna().getLaboratorioDenominacion();
				expirationDate = new SimpleDateFormat("yyyy-MM-dd").format(ad
						.getVacuna().getFechaVencimiento());

				route = ad.getVia();

				adminSiteDescripcion = ad.getParteCuerpo();

				doseQuantity = ad.getCantidadAdministracion() + "";
				doseUnidadMedida = ad.getUnidades();

				VXU_V04_ORCRXARXROBXNTE vxuV04 = vxu
						.getORCRXARXROBXNTE(counter);

				Calendar cal = Calendar.getInstance();
				try {
					cal.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(date));
					int year = cal.get(Calendar.YEAR);
					int month = cal.get(Calendar.MONTH) + 1;
					int day = cal.get(Calendar.DAY_OF_MONTH);
					vxuV04.getRXA().getRxa3_DateTimeStartOfAdministration()
							.getTimeOfAnEvent()
							.setDatePrecision(year, month, day);

					vxuV04.getRXA().getRxa10_AdministeringProvider(0)
							.getIDNumber().setValue(performerId);
					vxuV04.getRXA().getRxa10_AdministeringProvider(0)
							.getGivenName().setValue(performerName);
					vxuV04.getRXA().getRxa10_AdministeringProvider(0)
							.getFamilyLastName().getFamilyName()
							.setValue(performerLastName);

					vxuV04.getRXA().getRxa11_AdministeredAtLocation()
							.getCountry().setValue("PRY");
					vxuV04.getRXA().getRxa11_AdministeredAtLocation()
							.getStreetAddress().setValue(locationAddress);
					vxuV04.getRXA().getRxa11_AdministeredAtLocation()
							.getFacility().getNamespaceID().setValue(location);

					vxuV04.getRXA().getRxa5_AdministeredCode().getCe2_Text()
							.setValue(vaccineType);
					vxuV04.getRXA().getRxa15_SubstanceLotNumber(0)
							.setValue(lotNumber);
					vxuV04.getRXA().getRxa17_SubstanceManufacturerName(0)
							.getCe2_Text().setValue(manufacturer);

					cal.setTime(new SimpleDateFormat("yyyy-MM-dd")
							.parse(expirationDate));
					year = cal.get(Calendar.YEAR);
					month = cal.get(Calendar.MONTH) + 1;
					day = cal.get(Calendar.DAY_OF_MONTH);
					vxuV04.getRXA().getRxa16_SubstanceExpirationDate(0)
							.getTs1_TimeOfAnEvent()
							.setDatePrecision(year, month, day);

					vxuV04.getRXR().getRxr2_Site().getCe2_Text()
							.setValue(adminSiteDescripcion);
					vxuV04.getRXR().getRxr1_Route().getCe2_Text()
							.setValue(route);

					vxuV04.getRXA().getRxa6_AdministeredAmount()
							.setValue(doseQuantity);

					vxuV04.getRXA().getRxa7_AdministeredUnits().getCe2_Text()
							.setValue(doseUnidadMedida);

					if (!(ad.getDescripcionObservacion() != null
							|| ad.getDescripcionObservacion().equals("") || ad
							.getDescripcionObservacion().equals(
									"No tuvo reaccion"))) {
						vxuV04.getRXA().getAdministrationNotes(0).getCe2_Text()
								.setValue(ad.getDescripcionObservacion());
					}
					counter++;

					String st = vxu.encode();
					listaReturn.add(st);
					System.out.println(st);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

		return listaReturn;

	}

	public List<String> administracionToHL7V2(Administracion ad)
			throws HL7Exception, IOException {
		// instanciar clase del v2 y parsear el string

		// //////////////////////////////////////////////////
		// First, a message object is constructed
		HapiContext context = new DefaultHapiContext();

		context.setModelClassFactory(new CanonicalModelClassFactory(
				VXU_V04.class));
		// Message hapiMsg =
		// context.getPipeParser().parse(mensaje.getMensaje());

		VXU_V04 vxu = new VXU_V04();

		// declaraci�n de variables
		String identifier;
		String date;// RXA-3-date/time start of administration
		String vaccineType;// RXA-5-administered code
		String subject;// PID-3-patient ID list
		String reported;// RXA-9
		String performerId, performerName, performerLastName;// RXA-10-administering
																// provider
		String requesterId, requesterName, requesterLastName;
		String requester;// ORC-12-ordering provider
		String manufacturer;// RXA-17-substance manufacturer name
		String location;// RXA-27-administered-at (or RXA-11-administered-at
						// location, deprecated as of v2.7)
		String lotNumber;// RXA-15-substance lot number
		String expirationDate;// RXA-16-substance expiration date
		String adminSiteId, adminSiteDescripcion;// RXR-2-administration site
		String route;// RXR-1-route
		String doseQuantity; // RXA-6-administered amount / RXA-7.1-administered
		String doseUnidadMedida; // units.code
		String refusalReasonId;// RXA-18-2
		String refusalReasonDescripcion;
		String refusalReasonCodDescrip;
		String reaction; // OBX-3
		String reactionDate;// OBX-14 (ideally this would be reported in an IAM
							// segment, but IAM is not part of the V2.x VXU
							// message - most likely would appear in OBX
							// segments if at all
		// )
		String reactionDetail;// OBX-5
		String encoded = null;
		Integer indice = 0;

		// ///////////////////////////////////////////////////
		// //////////////////////////////////////////////////
		date = new SimpleDateFormat("yyyy-MM-dd").format(ad
				.getFechaAdministracion());
		vaccineType = ad.getVacuna().getNombreVacuna();
		subject = ad.getPaciente().getId() + "";
		reported = null;
		performerId = ad.getProfesional().getNroRegistro();
		performerName = ad.getProfesional().getNombres();
		performerLastName = ad.getProfesional().getApellidos();

		manufacturer = ad.getVacuna().getLaboratorioDenominacion();
		location = ad.getCentroAsistencial().getNombre();
		lotNumber = ad.getVacuna().getLote();
		expirationDate = new SimpleDateFormat("yyyy-MM-dd").format(ad
				.getVacuna().getFechaVencimiento());
		adminSiteDescripcion = ad.getParteCuerpo();
		route = ad.getVia();
		doseQuantity = ad.getCantidadAdministracion() + "";
		doseUnidadMedida = ad.getUnidades();

		// de refusal se supuso que no hubo inconveniente en todas las
		// aplicaciones

		ArrayList<String> listaReturn = new ArrayList<String>();

		int i = vxu.getORCRXARXROBXNTEReps();
		int cont = 0;
		System.out.println(i);
		List<VXU_V04_ORCRXARXROBXNTE> l = new ArrayList<VXU_V04_ORCRXARXROBXNTE>();
		//
		//
		// for (VXU_V04_ORCRXARXROBXNTE seg : l) {
		// // /////////////////////////////////////////////////////
		// // instanciar clase del FHIR e ir agregando los datos
		//
		// VXU_V04_ORCRXARXROBXNTE vxuv04ORCRXARXROBXNTE = new
		// VXU_V04_ORCRXARXROBXNTE(vxu, null);
		//
		// // identifie r
		// identifier = (indice + 1) + ""; // ver de donde sacar esto
		// // vacuna.addIdentifier().setValue(identifier);
		//
		// // date
		// date = seg.getRXA().getDateTimeStartOfAdministration().encode();
		// DateTimeDt fecha = new DateTimeDt();
		// fecha.setValueAsString(date);
		// vacuna.setDate(fecha);
		//
		// System.out.println(vacuna.getDate().toString());
		//
		// // vaccineType
		// vaccineType = seg.getRXA().getAdministeredCode().getCe2_Text()
		// .encode();
		// // subject
		// subject = vxu.getPID().getPatientID().encode(); // ver como
		// // identificar
		// // al paciente,
		// // con su id
		// // nomas?
		// ResourceReferenceDt referenciaPaciente = new ResourceReferenceDt();
		// referenciaPaciente.setReference(URLToSystem + "/FHIR/Patient/"
		// + subject);
		// vacuna.setSubject(referenciaPaciente);
		//
		// // refuseIndicator -Sin correspondencia-
		//
		// // reported
		// String rep = ""; // se extrae cada reporte
		// CE[] ces = seg.getRXA().getAdministrationNotes();
		// for (CE ces_one : ces) {
		// rep = rep + " " + ces_one.getCe1_Identifier() + " "
		// + ces_one.getCe2_Text();
		// }
		// reported = rep;
		// vacuna.setReported(true);
		//
		// // performer
		// XCN[] xcns = seg.getRXA().getAdministeringProvider();
		// XCN xcnFirst = seg.getRXA().getAdministeringProvider(0);
		// for (XCN xcn_one : xcns) {
		// rep = rep + " " + xcn_one.getXcn2_FamilyLastName() + " " + " ";
		// }
		// performerId="";
		// performerName = xcnFirst.getGivenName().encode();
		//
		// performerLastName = xcnFirst.getXcn2_FamilyLastName().encode();
		//
		// performerId = xcnFirst.getXcn1_IDNumber().encode();
		// // if (xcns != null) {
		// // XCN xcnFirst = xcns[0];
		// // performerName = xcnFirst.getGivenName().encode();
		// //
		// // performerLastName = xcnFirst.getXcn2_FamilyLastName()
		// // .encode();
		// //
		// // performerId = xcnFirst.getXcn1_IDNumber().encode();
		// // }
		// //
		// ResourceReferenceDt referenciaEspecialista = new
		// ResourceReferenceDt();
		// referenciaEspecialista.setReference(URLToSystem
		// + "/FHIR/Practitioner/" + performerId);
		// vacuna.setPerformer(referenciaEspecialista);
		//
		// // requester
		// ORC orc=seg.getORC();
		// XCN requesterSeg=orc.getOrc12_OrderingProvider(0);
		// requesterName = requesterSeg.getName().toString();
		// requesterLastName = requesterSeg.getFamilyLastName().encode();
		// requesterId = requesterSeg.getIDNumber().encode();
		// ResourceReferenceDt referenciaSolicitante = new
		// ResourceReferenceDt();
		// referenciaSolicitante.setReference(URLToSystem
		// + "/FHIR/Practitioner/" + requesterId);
		// vacuna.setRequester(referenciaSolicitante);
		//
		// // manufacturer
		// CE ceFirst=seg.getRXA().getRxa17_SubstanceManufacturerName(0);
		//
		// manufacturer = ceFirst.getCe1_Identifier().encode();
		// ResourceReferenceDt referenciaFabricante = new ResourceReferenceDt();
		// referenciaFabricante.setReference(URLToSystem
		// + "/FHIR/Manufacturer/" + manufacturer);
		// vacuna.setManufacturer(referenciaFabricante);
		//
		// // location
		//
		// location
		// =seg.getRXA().getRxa11_AdministeredAtLocation().getFacility().encode();//
		// para verison 2.7 se
		// // usa RXA 27
		// ResourceReferenceDt referenciaLocation = new ResourceReferenceDt();
		// referenciaLocation.setReference(URLToSystem + "/FHIR/Location/"
		// + location);
		// vacuna.setManufacturer(referenciaLocation);
		//
		// // lotNumber
		// lotNumber = seg.getRXA().getRxa15_SubstanceLotNumber(0).encode();
		// vacuna.setLotNumber(lotNumber);
		//
		// // expirationDate
		// expirationDate =
		// seg.getRXA().getRxa16_SubstanceExpirationDate(0).encode();
		// DateDt fechaExpiracion = new DateDt();
		// if(!expirationDate.equals("")){
		// fechaExpiracion.setValueAsString(expirationDate);
		// }
		// vacuna.setExpirationDate(fechaExpiracion);
		//
		// // site
		// adminSiteId = seg.getRXR().getRxr2_Site().getIdentifier().encode();
		// adminSiteDescripcion =
		// seg.getRXR().getRxr2_Site().getCe2_Text().encode();
		// CodingDt codificacion = new CodingDt(); // para definir codigos en
		// // FHIR
		// /*
		// * Un bodySite tiene como tipo de dato un concepto codificable Un
		// * concepto codificable se compone de un texto y una lista de
		// * codigos correspondientes a alg�n est�ndar a los que puede
		// * mapearse
		// */
		// CodeableConceptDt sitioCuerpo = new CodeableConceptDt();
		// sitioCuerpo.setText(adminSiteDescripcion); // Descripcion del
		// // atributo codificable
		//
		// CodingDt codificacionBody1 = new CodingDt(); // Tipo de dato para
		// // definir codigos
		// codificacionBody1.setCode(adminSiteId);// valor del codigo
		// ArrayList<CodingDt> listaCodigoBody = new ArrayList<CodingDt>();
		// listaCodigoBody.add(codificacionBody1);// el BodySite se puede
		// // mapear a mas de un
		// // codigo, por lo tanto se
		// // mete en una lista
		//
		// String adminSiteIdHL7 =
		// seg.getRXR().getRxr2_Site().getCe3_NameOfCodingSystem().encode();
		// CodingDt codificacionBody2 = new CodingDt(); // Tipo de dato para
		// // definir codigos
		// codificacionBody1.setCode(adminSiteIdHL7);// valor del codigo
		// listaCodigoBody.add(codificacionBody2);
		//
		// sitioCuerpo.setCoding(listaCodigoBody);
		// vacuna.setSite(sitioCuerpo); // finalmente se setea el valor del
		// // BodySite
		//
		// // dose
		// doseQuantity = seg.getRXA().getRxa6_AdministeredAmount().encode();
		// doseUnidadMedida = seg.getRXA().getRxa7_AdministeredUnits().encode();
		// QuantityDt dosis = new QuantityDt();
		// dosis.setValue(Double.valueOf(doseQuantity));
		// dosis.setUnits(doseUnidadMedida);
		// vacuna.setDoseQuantity(dosis);
		//
		// // explanation.reason -Sin correspondencia-
		//
		// // explanation.refusalReason
		// refusalReasonId =
		// seg.getRXA().getRxa18_SubstanceRefusalReason(0).getCe1_Identifier().encode();
		// refusalReasonDescripcion =
		// seg.getRXA().getRxa18_SubstanceRefusalReason(0).getCe2_Text().encode();
		// refusalReasonCodDescrip =
		// seg.getRXA().getRxa18_SubstanceRefusalReason(0).getCe3_NameOfCodingSystem().encode();
		// Explanation vacunaExplicacion = new Explanation();
		// CodeableConceptDt rechazoRazones = new CodeableConceptDt();
		// rechazoRazones.setText(refusalReasonDescripcion);
		// CodingDt codigoRechazo = new CodingDt();
		// codigoRechazo.setCode(refusalReasonId);
		// codigoRechazo.setDisplay(refusalReasonDescripcion);
		//
		// ArrayList<CodingDt> listaCodigoRazonRechazo = new
		// ArrayList<CodingDt>();
		// listaCodigoRazonRechazo.add(codigoRechazo);// las razones de rechazo
		// // se puede mapear a mas
		// // de un codigo, por lo
		// // tanto se mete en una
		// // lista
		// rechazoRazones.setCoding(listaCodigoRazonRechazo);
		//
		// ArrayList<CodeableConceptDt> listaRechazo = new
		// ArrayList<CodeableConceptDt>();
		// listaRechazo.add(rechazoRazones);
		//
		// vacunaExplicacion.setRefusalReason(listaRechazo);
		// vacuna.setExplanation(vacunaExplicacion);
		//
		// // reaction.date
		// reaction =
		// seg.getOBXNTE().getOBX().getObx3_ObservationIdentifier().getCe2_Text().encode();
		// reactionDate =
		// seg.getOBXNTE().getOBX().getObx14_DateTimeOfTheObservation().encode();
		//
		// Reaction reaccionVacuna = new Reaction();
		// DateTimeDt fechaReaccion = new DateTimeDt();
		// if(!reactionDate.equals("")){
		// fechaReaccion.setValueAsString(reactionDate);
		// }
		//
		// reaccionVacuna.setDate(fechaReaccion);
		//
		//
		// // reaction.detail
		// reactionDetail =
		// seg.getOBXNTE().getOBX().getObx14_DateTimeOfTheObservation().encode();
		// ResourceReferenceDt referenciaRecurso = new ResourceReferenceDt();
		// referenciaRecurso.setReference(URLToSystem + "/FHIR/Recurso/id");//
		// generico
		// // porque
		// // detail
		// // en
		// // FHIR
		// // apunta
		// // a
		// // "any"
		// // Resource
		// // ???
		//
		// referenciaRecurso.setDisplay(reactionDetail);
		// reaccionVacuna.setDetail(referenciaRecurso);
		//
		// // reaction.reported -Sin correspondencia-
		// // vaccinationProtocol -Sin correspondencia-
		// // doseSequence -Sin correspondencia-
		// // description -Sin correspondencia-
		// // authority -Sin correspondencia-
		// // series -Sin correspondencia-
		// // seriesDoses -Sin correspondencia-
		// // doseTarget -Sin correspondencia-
		// // doseStatus -Sin correspondencia-
		// // doseStatusReason -Sin correspondencia-
		//
		// // to XML
		// indice++;
		// encoded = ctx.newXmlParser().encodeResourceToString(vacuna);
		// listaReturn.add(encoded);
		// }

		// return listaReturn;
		return null;
	}

	/* Method to READ all the administracions */
	public List<Administracion> listAdministracions() {
		Session session = factory.openSession();
		Transaction tx = null;
		ArrayList<Administracion> listaAL = new ArrayList<Administracion>();
		try {
			tx = session.beginTransaction();
			Query q = session.createQuery("FROM Administracion");
			List administracions = q.list();
			for (Iterator iterator = administracions.iterator(); iterator
					.hasNext();) {
				Administracion administracion = (Administracion) iterator
						.next();
				// System.out.println("Unidades: " +
				// administracion.getUnidades());
				listaAL.add(administracion);
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return listaAL;
	}

	/* Method to READ all the administracions */
	public List<Paciente> listPacientes() {
		Session session = factory.openSession();
		Transaction tx = null;
		ArrayList<Paciente> listaAL = new ArrayList<Paciente>();
		try {
			tx = session.beginTransaction();
			Query q = session.createQuery("FROM Paciente");
			List administracions = q.list();
			for (Iterator iterator = administracions.iterator(); iterator
					.hasNext();) {
				Paciente paciente = (Paciente) iterator.next();
				// System.out.println("Unidades: " +
				// administracion.getUnidades());
				listaAL.add(paciente);
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return listaAL;
	}
	
	// retorna un profesional a partir de un objeto vacuna
	public Profesional getProfesionalByExample(Profesional profExample) {

			Session session = (Session) factory.openSession();
			Example profesionalExam = Example.create(profExample).enableLike();
			Criteria criteria = session.createCriteria(Profesional.class).add(
					profesionalExam);
			List<Profesional> lista = criteria.list();
			session.close();
			if(lista!=null && lista.size()>0)
				return lista.get(0);
			else
				return null;

		}

	// retorna un profesional a partir de su id
	public Profesional getProfesional(String id) {
		Integer idInt = Integer.parseInt(id);
		Session session = factory.openSession();
		Transaction tx = null;
		Profesional prof = null;
		try {
			tx = session.beginTransaction();
			prof = (Profesional) session.get(Profesional.class, idInt);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return prof;

	}

	// retorna un vacuna a partir de un objeto vacuna
	public List<Vacuna> getVacunaByExample(Vacuna vacExample) {

		Session session = (Session) factory.openSession();
		Example addressExample = Example.create(vacExample).enableLike();
		Criteria criteria = session.createCriteria(Vacuna.class).add(
				addressExample);
		List<Vacuna> lista = criteria.list();
		session.close();
		return lista;

	}

	
	// retorna un vacuna a partir de un objeto vacuna
	public Paciente getPacienteByExample(Paciente paciente) {

		Session session = (Session) factory.openSession();
		Example pacExample = Example.create(paciente).enableLike();
		Criteria criteria = session.createCriteria(Paciente.class).add(
				pacExample);
		List<Paciente> lista = criteria.list();
		session.close();
		
		if(lista!=null && lista.size()>0)
			return lista.get(0);
		else
			return null;
		
		
	}

	// retorna una administracion a partir de un objeto administracion
	public List<Vacuna> getAdministracionByExample(Administracion adExample) {

		Session session = (Session) factory.openSession();
		Example addressExample = Example.create(adExample).enableLike();
		Criteria criteria = session.createCriteria(Administracion.class).add(
				addressExample);
		List<Vacuna> lista = criteria.list();
		session.close();
		return lista;

	}

	// retorna un centro asistencial a partir de un objeto vacuna
	public List<CentroAsistencial> getCentroAsistencialByExample(
			CentroAsistencial caExample) {

		Session session = (Session) factory.openSession();
		Example addressExample = Example.create(caExample).enableLike();
		Criteria criteria = session.createCriteria(CentroAsistencial.class)
				.add(addressExample);
		List<CentroAsistencial> lista = criteria.list();
		session.close();
		return lista;

	}

	/* Method to UPDATE salary for an administracion */
	public void updateAdministracion(Integer AdministracionID) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Administracion administracion = (Administracion) session.get(
					Administracion.class, AdministracionID);
			session.update(administracion);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/* Method to DELETE an administracion from the records */
	// public void deleteAdministracion(Integer AdministracionID) {
	// Session session = factory.openSession();
	// Transaction tx = null;
	// try {
	// tx = session.beginTransaction();
	// Administracion administracion = (Administracion)
	// session.get(Administracion.class,
	// AdministracionID);
	// session.delete(administracion);
	// tx.commit();
	// } catch (HibernateException e) {
	// if (tx != null)
	// tx.rollback();
	// e.printStackTrace();
	// } finally {
	// session.close();
	// }
	// }
}