package clienteRest;

import java.util.HashMap;
import java.util.Set;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class JerseyClient {

	public static void HTTPget() {
		try {

			Client client = Client.create();

			WebResource webResource = client
					.resource("http://54.197.122.119/rest/ubicaciones");

			// http://control-controlador.rhcloud.com/rest/ubicaciones
			// http://localhost:8080/RESTfulExample/rest/json/metallica/get

			ClientResponse response = webResource.accept("application/json")
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			String output = response.getEntity(String.class);

			System.out.println("Output from Server .... \n");
			System.out.println(output);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String HTTPget(HashMap<String, String> list) {
		String retorno = null;
		try {

			Client client = Client.create();

			WebResource webResource = client
					.resource("http://192.168.1.20:8080/RESTfulExample/rest/hl7/v231");

			// http://control-controlador.rhcloud.com/rest/ubicaciones
			// http://localhost:8080/RESTfulExample/rest/json/metallica/get

			ClientResponse response = webResource.accept("application/json")
					.get(ClientResponse.class);

			// se extra la lista de parametros
			Set<String> keys = list.keySet();
			for (String k : keys) {
				webResource.queryParam(k, list.get(k) + "");
			}

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			retorno = response.getEntity(String.class);

			System.out.println("Output from Server .... \n");
			System.out.println(retorno);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;

	}

	public static void HTTPPost() {
		try {

			Client client = Client.create();

			WebResource webResource = client
					.resource("http://localhost:8080/RESTfulExample/rest/json/metallica/post");

			String input = "{\"singer\":\"Metallica\",\"title\":\"Fade To Black\"}";

			ClientResponse response = webResource.type("application/json")
					.post(ClientResponse.class, input);

			if (response.getStatus() != 201) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			System.out.println("Output from Server .... \n");
			String output = response.getEntity(String.class);
			System.out.println(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}

	public static void HTTPPost(String s) {
		try {

			Client client = Client.create();

			WebResource webResource = client
					.resource("http://posttestserver.com/post.php");

			String input = s;

			ClientResponse response = webResource.type("application/json")
					.post(ClientResponse.class, input);

			if (response.getStatus() != 201) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			System.out.println("Output from Server .... \n");
			String output = response.getEntity(String.class);
			System.out.println(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
}