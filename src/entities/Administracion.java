package entities;

import java.io.Serializable;

import javax.persistence.*;

import entities.CentroAsistencial;
import entities.Paciente;
import entities.Profesional;
import entities.Vacuna;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The persistent class for the administracion database table.
 * 
 */
@Entity
@NamedQuery(name = "Administracion.findAll", query = "SELECT a FROM Administracion a")
public class Administracion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "cantidad_administracion")
	private BigDecimal cantidadAdministracion;

	@Column(name = "descripcion_observacion")
	private String descripcionObservacion;

	private Boolean eliminado;

	@Column(name = "fecha_administracion")
	private Timestamp fechaAdministracion;

	@Column(name = "parte_cuerpo")
	private String parteCuerpo;

	private String unidades;
	
	@Column(name = "via")
	private String via;

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	// bi-directional many-to-one association to CentroAsistencial
	@ManyToOne 
	@JoinColumn(name = "id_centro_asistencial")
	private CentroAsistencial centroAsistencial;

	// bi-directional many-to-one association to Paciente
	@ManyToOne 
	@JoinColumn(name = "id_paciente")
	private Paciente paciente;

	// bi-directional many-to-one association to Profesional
	@ManyToOne 
	@JoinColumn(name = "id_profesional")
	private Profesional profesional;

	// bi-directional many-to-one association to Vacuna
	@ManyToOne 
	@JoinColumn(name = "id_vacuna")
	private Vacuna vacuna;

	public Administracion() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getCantidadAdministracion() {
		return this.cantidadAdministracion;
	}

	public void setCantidadAdministracion(BigDecimal cantidadAdministracion) {
		this.cantidadAdministracion = cantidadAdministracion;
	}

	public String getDescripcionObservacion() {
		return this.descripcionObservacion;
	}

	public void setDescripcionObservacion(String descripcionObservacion) {
		this.descripcionObservacion = descripcionObservacion;
	}

	public Boolean getEliminado() {
		return this.eliminado;
	}

	public void setEliminado(Boolean eliminado) {
		this.eliminado = eliminado;
	}

	public Timestamp getFechaAdministracion() {
		return this.fechaAdministracion;
	}

	public void setFechaAdministracion(Timestamp fechaAdministracion) {
		this.fechaAdministracion = fechaAdministracion;
	}

	public String getParteCuerpo() {
		return this.parteCuerpo;
	}

	public void setParteCuerpo(String parteCuerpo) {
		this.parteCuerpo = parteCuerpo;
	}

	public String getUnidades() {
		return this.unidades;
	}

	public void setUnidades(String unidades) {
		this.unidades = unidades;
	}

	public CentroAsistencial getCentroAsistencial() {
		return this.centroAsistencial;
	}

	public void setCentroAsistencial(CentroAsistencial centroAsistencial) {
		this.centroAsistencial = centroAsistencial;
	}

	public Paciente getPaciente() {
		return this.paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Profesional getProfesional() {
		return this.profesional;
	}

	public void setProfesional(Profesional profesional) {
		this.profesional = profesional;
	}

	public Vacuna getVacuna() {
		return this.vacuna;
	}

	public void setVacuna(Vacuna vacuna) {
		this.vacuna = vacuna;
	}
}