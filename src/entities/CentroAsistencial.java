package entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the centro_asistencial database table.
 * 
 */
@Entity
@Table(name="centro_asistencial")
@NamedQuery(name="CentroAsistencial.findAll", query="SELECT c FROM CentroAsistencial c")
public class CentroAsistencial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String ciudad;

	private String departamento;

	private String direccion;

	private Boolean eliminado;

	private String nombre;

//	//bi-directional many-to-one association to Administracion
//	@OneToMany(mappedBy="centroAsistencial")
//	private List<Administracion> administracions;

	public CentroAsistencial() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCiudad() {
		return this.ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDepartamento() {
		return this.departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Boolean getEliminado() {
		return this.eliminado;
	}

	public void setEliminado(Boolean eliminado) {
		this.eliminado = eliminado;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

//	public List<Administracion> getAdministracions() {
//		return this.administracions;
//	}
//
//	public void setAdministracions(List<Administracion> administracions) {
//		this.administracions = administracions;
//	}

//	public Administracion addAdministracion(Administracion administracion) {
//		getAdministracions().add(administracion);
//		administracion.setCentroAsistencial(this);
//
//		return administracion;
//	}
//
//	public Administracion removeAdministracion(Administracion administracion) {
//		getAdministracions().remove(administracion);
//		administracion.setCentroAsistencial(null);
//
//		return administracion;
//	}

}