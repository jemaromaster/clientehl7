package entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * The persistent class for the paciente database table.
 * 
 */
@Entity
@NamedQuery(name = "Paciente.findAll", query = "SELECT p FROM Paciente p")
public class Paciente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String apellidos;

	private Integer ci;

	private String nombres;

	// bi-directional many-to-one association to Administracion
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "paciente")
	private List<Administracion> administracions;

	public List<Administracion> getAdministracions() {
		return administracions;
	}

	public void setAdministracions(List<Administracion> administracions) {
		this.administracions = administracions;
	}

	public Paciente() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getCi() {
		return this.ci;
	}

	public void setCi(Integer ci) {
		this.ci = ci;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

}