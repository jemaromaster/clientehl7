package entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the profesional database table.
 * 
 */
@Entity
@NamedQuery(name="Profesional.findAll", query="SELECT p FROM Profesional p")
public class Profesional implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	private String apellidos;

	private String ci;

	private String nombres;

	@Column(name="nro_registro")
	private String nroRegistro;

	//bi-directional many-to-one association to Administracion
	@OneToMany(mappedBy="profesional")
	private List<Administracion> administracions;

	public Profesional() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCi() {
		return this.ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getNroRegistro() {
		return this.nroRegistro;
	}

	public void setNroRegistro(String nroRegistro) {
		this.nroRegistro = nroRegistro;
	}

	public List<Administracion> getAdministracions() {
		return this.administracions;
	}

	public void setAdministracions(List<Administracion> administracions) {
		this.administracions = administracions;
	}

	public Administracion addAdministracion(Administracion administracion) {
		getAdministracions().add(administracion);
		administracion.setProfesional(this);

		return administracion;
	}

	public Administracion removeAdministracion(Administracion administracion) {
		getAdministracions().remove(administracion);
		administracion.setProfesional(null);

		return administracion;
	}

}