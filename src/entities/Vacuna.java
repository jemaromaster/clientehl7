package entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the vacuna database table.
 * 
 */
@Entity
@NamedQuery(name="Vacuna.findAll", query="SELECT v FROM Vacuna v")
public class Vacuna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	private Boolean eliminado;

	@Column(name="fecha_adquisicion")
	private Timestamp fechaAdquisicion;

	@Column(name="fecha_vencimiento")
	private Timestamp fechaVencimiento;

	@Column(name="laboratorio_denominacion")
	private String laboratorioDenominacion;

	private String lote;

	@Column(name="nombre_vacuna")
	private String nombreVacuna;

	public Vacuna() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getEliminado() {
		return this.eliminado;
	}

	public void setEliminado(Boolean eliminado) {
		this.eliminado = eliminado;
	}

	public Timestamp getFechaAdquisicion() {
		return this.fechaAdquisicion;
	}

	public void setFechaAdquisicion(Timestamp fechaAdquisicion) {
		this.fechaAdquisicion = fechaAdquisicion;
	}

	public Timestamp getFechaVencimiento() {
		return this.fechaVencimiento;
	}

	public void setFechaVencimiento(Timestamp fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getLaboratorioDenominacion() {
		return this.laboratorioDenominacion;
	}

	public void setLaboratorioDenominacion(String laboratorioDenominacion) {
		this.laboratorioDenominacion = laboratorioDenominacion;
	}

	public String getLote() {
		return this.lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public String getNombreVacuna() {
		return this.nombreVacuna;
	}

	public void setNombreVacuna(String nombreVacuna) {
		this.nombreVacuna = nombreVacuna;
	}

}