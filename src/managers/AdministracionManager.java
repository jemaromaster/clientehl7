package managers;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entities.Administracion;

public class AdministracionManager {

	@PersistenceContext(unitName = "administracion")
	private EntityManager em;
	
	List<Administracion> list;

	public List<Administracion> getAllAdministracion() {
		try {
			list = new ArrayList<Administracion>();
			Query q = em.createQuery("SELECT e FROM Administracion e");
			list = (List<Administracion>) q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}
}
