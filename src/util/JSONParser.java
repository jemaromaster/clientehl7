package util;
 
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {
 
    InputStream is = null;
    JSONObject jObj = null;
    String json = "";
 
    // constructor
    public JSONParser() {
 
    }
 
    public JSONObject getJSONFromUrlSinN(String url, List<NameValuePair> params) {
    	 
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
           // httpPost.setEntity(new UrlEncodedFormEntity(params));
          //  httpPost.setParams(hp);
 
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            System.out.println("JSON:"+ json);
        } catch (Exception e) {
            System.out.println("Buffer Error:"+ "Error converting result " + e.toString());
        }
 
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);  

        } catch (JSONException e) {
            System.out.println("JSON Parser:"+"Error parsing data " + e.toString());
            
        }
 
        // return JSON String
        return jObj;
 
    }
    public JSONObject getJSONFromUrl(String url, List<NameValuePair> params) {
 
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
           // httpPost.setEntity(new UrlEncodedFormEntity(params));
          //  httpPost.setParams(hp);
 
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
            }
            is.close();
            json = sb.toString();
            System.out.println("JSON"+ json);
        } catch (Exception e) {
            System.out.println("Buffer Error"+ "Error converting result " + e.toString());
        }
 
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);  

        } catch (JSONException e) {
            System.out.println("JSON Parser"+ "Error parsing data " + e.toString());
            
        }
 
        // return JSON String
        return jObj;
 
    }
    
    public JSONObject putJSONUrl(String url, List<NameValuePair> params) {
   	 
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPut httpPut = new HttpPut(url);
            httpPut.setEntity(new UrlEncodedFormEntity(params));
//            httpPost.setParams(hp);
 
            HttpResponse httpResponse = httpClient.execute(httpPut);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
            }
            is.close();
            json = sb.toString();
            System.out.println("JSON:"+ json);
        } catch (Exception e) {
            System.out.println( "Error converting result " + e.toString());
        }
 
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);  

        } catch (JSONException e) {
            System.out.println("JSON Parser"+ "Error parsing data " + e.toString());
            
        }
 
        // return JSON String
        return jObj;
    }
    public JSONObject postJSONUrl(String url, List<NameValuePair> params) {
    	 
    	// Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
//            httpPost.setParams(hp);
 
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
            }
            is.close();
            json = sb.toString();
            System.out.println("JSON"+ json);
        } catch (Exception e) {
            System.out.println("Buffer Error"+ ":Error converting result " + e.toString());
        }
 
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);  

        } catch (JSONException e) {
            System.out.println("JSON Parser"+":"+ "Error parsing data " + e.toString());
            
        }
 
        // return JSON String
        return jObj;
    }
    
//    public JSONObject deleteJSONUrl(String url, List<NameValuePair> params) {
//   	 
//        // Making HTTP request
//        try {
//            // defaultHttpClient
//            DefaultHttpClient httpClient = new DefaultHttpClient();
//            HttpDelete httpDelete = new HttpDelete(url);
////            httpDelete.setEntity(new UrlEncodedFormEntity(params));
////            httpPost.setParams(hp);
// 
//            HttpResponse httpResponse = httpClient.execute(httpDelete);
//            HttpEntity httpEntity = httpResponse.getEntity();
//            is = httpEntity.getContent();
// 
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
// 
//        try {
//            BufferedReader reader = new BufferedReader(new InputStreamReader(
//                    is, "iso-8859-1"), 8);
//            StringBuilder sb = new StringBuilder();
//            String line = null;
//            while ((line = reader.readLine()) != null) {
//                sb.append(line + "n");
//            }
//            is.close();
//            json = sb.toString();
//            System.out.println("JSON", json);
//        } catch (Exception e) {
//            System.out.println("Buffer Error", "Error converting result " + e.toString());
//        }
// 
//        // try parse the string to a JSON object
//        try {
//            jObj = new JSONObject(json);  
//
//        } catch (JSONException e) {
//            System.out.println("JSON Parser", "Error parsing data " + e.toString());
//            
//        }
// 
//        // return JSON String
//        return jObj;
//    }
}