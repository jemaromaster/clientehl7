package util;

import java.util.ArrayList;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.group.VXU_V04_ORDER;
import ca.uhn.hl7v2.model.v25.message.VXU_V04;
import ca.uhn.hl7v2.parser.EncodingNotSupportedException;
import ca.uhn.hl7v2.parser.Parser;

public class ParseMessages {

	public static void main(String[] args) throws HL7Exception {
		String msg = "MSH|^~\\&||MA0000||GA0000|199705221610||VXR^V03^V03|19970522MA53|T|2.3.1|||NE|AL|<CR>"
				+ "MSA|AA|19970522GA40|<CR>"
				+ "QRD|199705221605|R|I|19970522GA05|||25^RD|^KENNEDY^JOHN^FITZGERALD^JR|VXI^VACCINE"
				+ "INFORMATION^HL70048|^SIIS|<CR>"
				+ "QRF|MA0000||||256946789~19900607~MA~MA99999999~88888888~KENNEDY^JACQUELINE^"
				+ "LEE~BOUVIER~898666725~KENNEDY^JOHN^FITZGERALD~822546618|<CR>"
				+ "PID|||1234^^^^SR^~1234-12^^^^LR^~3872^^^^MR~221345671^^^^SS^~430078856^^^^MA^"
				+ "||KENNEDY^JOHN^FITZGERALD^JR^^^L|BOUVIER^^^^^^M|19900607|M|KENNEDY^BABY BOY^^^^^^"
				+ "B|2106-3^WHITE^HL70005|123 MAIN ST^APT 3B^LEXINGTON^MA^00210^^M^MSA"
				+ "CODE^MA034~345 ELM ST^^BOSTON^MA^00314^^BDL~^^^^^^BR^^MA002||(617)555-1212^PRN"
				+ "^PH^^^617^5551212^^||EN^ENGLISH^HL70296^^^|||||||N^NOT HISPANIC OR LATINO^HL70189^2186-"
				+ "5^NOT HISPANIC OR LATINO^CDCRE1|CHILDREN’S HOSPITAL|<CR>"
				+ "PD1|||CHILDREN’S CLINIC^L^1234^^^^FI^LEXINGTON HOSPITAL&5678&XX|12345^WELBY^"
				+ "MARCUS^^^DR^MD^^^L^^^DN|||||||03^REMINDER/RECALL - NO CALLS^HL70215|Y|19900607"
				+ "|||A|19900607|19900607|<CR>"
				+ "NK1|1|KENNEDY^JACQUELINE^LEE|MTH^MOTHER^HL70063||||||||||||||||||||||||||||||898666725^^^^SS|<CR>"
				+ "NK1|2|KENNEDY^JOHN^FITZGERALD|FTH^FATHER^HL70063||||||||||||||||||||||||||||||822546618^^^^SS|<CR>"
				+ "PV1||R||||||||||||||||||V02^19900607~H02^19900607|<CR>"
				+ "RXA|0|1|19900607|19900607|08^HEPB-PEDIATRIC/ADOLESCENT^CVX^90744^HEPB-PEDATRIC"
				+ "/ADOLESCENT^C4|.5|ML^^ISO+||03^HISTORICAL INFORMATION - FROM PARENT’S WRITTEN"
				+ "RECORD^NIP0001|^JONES^LISA|^^^CHILDREN’S"
				+ "HOSPITAL||5|MCG^^ISO+|MRK12345|199206|MSD ^MERCK^MVX|<CR>"
				+ "RXA|0|0|19901207|19901207|20^DTAP^CVX|.5|ML^^ISO+|||1234567891^O’BRIAN^ROBERT^A^^DR^"
				+ "MD|^^^CHILD HEALTHCARE CLINIC^^^^^101 MAIN STREET^^BOSTON^MA||||W22532806|19901230|"
				+ "PMC^PASTEUR MERIEUX CONNAUGHT^MVX|00^PARENTAL DECISION^NIP002||RE|<CR>"
				+ "OBX|1|TS|29768-9^DATE VACCINE INFORMATION STATEMENT PUBLISHED^LN||19900605||||||F|<CR>"
				+ "OBX|2|TS|29769-7^DATE VACCINE INFORMATION STATEMENT PRESENTED^LN||19901207||||||F|<CR>"
				+ "RXA|0|1|19910907|19910907|50^DTAP-HIB^CVX^90721^DTAP-HIB^C4|.5|ML^^ISO+||00^NEW"
				+ "IMMUNIZATION RECORD^NIP001|1234567890^SMITH^SALLY^S^^^^^^^^^VEI~"
				+ "1234567891^O’BRIAN^ROBERT^A^^DR^MD^^^^^^OEI|^^^CHILD HEALTHCARE CLINIC^^^^^101"
				+ "MAIN STREET^^BOSTON^MA||||W46932777|199208|PMC^PASTEUR MERIEUX CONNAUGHT"
				+ "^MVX|||CP|A|19910907120030|<CR>"
				+ "RXR|IM^INTRAMUSCULAR^HL70162|LA^LEFT ARM^HL70163|<CR>"
				+ "OBX|1|NM|30936-9^DTAP/DTP DOSE COUNT IN COMBINATION VACCINE^LN||4||||||F|<CR>"
				+ "OBX|2|NM|30938-5^HAEMOPHILUS INFLUENZAE TYPE B (HIB) DOSE COUNT IN COMBINATION"
				+ "VACCINE^LN||4||||||F|<CR>"
				+ "RXA|0|1|19910907|19910907|03^MMR^CVX|.5|ML^^ISO+|||1234567890^SMITH^SALLY^S^^^^^^^^^VEI"
				+ "~1234567891^O’BRIAN^ROBERT^A^^DR^MD^^^^^^OEI|^^^CHILD HEALTHCARE CLINIC^^^^^101"
				+ "MAIN STREET^^BOSTON^MA||||W2348796456|19920731|MSD^MERCK^MVX|<CR>"
				+ "RXR|SC^SUBCUTANEOUS^HL70162|LA^LEFT ARM^HL70163|<CR>"
				+ "RXA|0|5|19950520|19950520|20^DTAP^CVX|.5|ML^^ISO+|||1234567891^O’BRIAN^ROBERT^A^^DR^M"
				+ "D|^^^CHILD HEALTHCARE CLINIC^^^^^101 MAIN STREET^^BOSTON^MA||||W22532806|19950705|"
				+ "PMC^PASTEUR MERIEUX CONNAUGHT ^MVX|<CR>"
				+ "RXR|IM^INTRAMUSCULAR^HL70162|LA^LEFT ARM^HL70163|<CR>"
				+ "RXA|0|2|19950520|19950520|03^MMR^CVX|.5|ML^^ISO+|||1234567891^O’BRIAN^ROBERT^A^^DR^M"
				+ "D|^^^CHILD HEALTHCARE CLINIC^^^^^101 MAIN STREET^^BOSTON^MA||||W2341234567|19950630|"
				+ "MSD^ MERCK^MVX|<CR>"
				+ "RXR|SC^SUBCUTANEOUS^HL70162|LA^LEFT ARM^HL70163|<CR>"
				+ "OBX||FT|30948-4^VACCINATION ADVERSE EVENT AND TREATMENT, IF"
				+ "ANY^LN||ANAPHYLAXIS||||||F|<CR>"
				+ "NTE|||PATIENT DEVELOPED HIGH FEVER APPROX 3 HRS AFTER VACCINE INJECTION|<CR>"
				+ "NTE|||VAERS FORM SUBMITTED BY PROVIDER|<CR>"
				+ "RXA|0|1|19960415|19960415|96^TST-PPD INTRADERMAL^CVX|5|TU|<CR>"
				+ "OBX||NM|1648-5^TUBERCULOSIS REACTION WHEAL 3D POST 5 TU ID^LN||1|MM||N|||F|||19960418|<CR>";

		/*
		 * 62 * The HapiContext holds all configuration and provides factory
		 * methods for obtaining 63 * all sorts of HAPI objects, e.g. parsers.
		 * 64
		 */
		HapiContext context = new DefaultHapiContext();

		/*
		 * 68 * A Parser is used to convert between string representations of
		 * messages and instances of 69 * HAPI's "Message" object. In this case,
		 * we are using a "GenericParser", which is able to 70 * handle both XML
		 * and ER7 (pipe & hat) encodings. 71
		 */
		Parser p = context.getGenericParser();

		Message hapiMsg;
		try {
			// The parse method performs the actual parsing
			hapiMsg = p.parse(msg);
		} catch (EncodingNotSupportedException e) {
			e.printStackTrace();
			return;
		} catch (HL7Exception e) {
			e.printStackTrace();
			return;
		}

		/*
		 * 87 * This message was an ADT^A01 is an HL7 data type consisting of
		 * several components1, so we 88 * will cast it as such. The ADT_A01
		 * class extends from Message, providing specialized 89 * accessors for
		 * ADT^A01's segments. 90 * 91 * HAPI provides several versions of the
		 * ADT_A01 class, each in a different package (note 92 * the import
		 * statement above) corresponding to the HL7 version for the message. 93
		 */
		System.out.println(hapiMsg.toString());
		VXU_V04 adtMsg = (VXU_V04) hapiMsg;

		ca.uhn.hl7v2.model.v25.segment.MSH msh = adtMsg.getMSH();

		msg.toString();
		// Retrieve some data from the MSH segment
		// String msgType = msh.getMessageType().getMessageType().getValue();
		// String msgTrigger =
		// msh.getMessageType().getTriggerEvent().getValue();

		// Prints "ADT A01"
		System.out.println(msg.toString());

		ArrayList<VXU_V04_ORDER> lista = (ArrayList<VXU_V04_ORDER>) adtMsg
				.getORDERAll();
		for (VXU_V04_ORDER orden : lista) {
			System.out.println(orden.getRXA()
					.getRxa3_DateTimeStartOfAdministration());
			System.out.println(orden.getRXA().getRxa5_AdministeredCode());
			System.out.println(adtMsg.getPID().getPatientIdentifierListReps());
			System.out.println(" ");

			System.out.println(orden.getRXA().getRxa9_AdministrationNotes(0));
			System.out
					.println(orden.getRXA().getRxa10_AdministeringProvider(0));

		}

		/*
		 * 106 * Now let's retrieve the patient's name from the parsed message.
		 * 107 * 108 * PN is an HL7 data type consisting of several components,
		 * such as 109 * family name, given name, etc. 110
		 */
		// PN patientName = adtMsg.getPID().getPatientName();
		//
		// // Prints "SMITH"
		// String familyName = patientName.getFamilyName().getValue();
		// System.out.println(familyName);

	}
}
