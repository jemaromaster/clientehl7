package util;
 
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import entities.Administracion;

 
public class UserFunctions {
     
    private JSONParser jsonParser;
     
    // Testing in localhost using wamp or xampp
    // use http://10.0.2.2/ to connect to your localhost ie http://localhost/
    private static String loginURL = "http://www.barreras.altervista.org/controlador/";
    private static String registerURL = "http://www.barreras.altervista.org/controlador/";
     
    private static String login_tag = "login";
    private static String register_tag = "register";
    private static String delete_tag = "delete";
    private static String twitter_tag = "twitter";

    // constructor
    public UserFunctions(){
        jsonParser = new JSONParser();
    }
     
    /**
     * function make Login Request
     * @param email
     * @param password
     * */
     
    
    public JSONObject getAllUbicaciones(String fecha){
        // Building Parameters
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tag", login_tag));
        params.add(new BasicNameValuePair("fecha", fecha));
        JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
        // return json
        // Log.e("JSON", json.toString());
        return json;
    }
    /**
     * function make Login Request
     * @param name
     * @param email
     * @param password
     * */
    public JSONObject postHL7MessagesVXU(String s){
        // Building Parameters
    	
        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        params.add(new BasicNameValuePair("tag", register_tag));
//        params.add(new BasicNameValuePair("longitud", (String) ubicacion.get("longitud")));
//        params.add(new BasicNameValuePair("latitud", (String) ubicacion.get("latitud")));   
//        params.add(new BasicNameValuePair("imei", (String) ubicacion.get("imei")));
//        params.add(new BasicNameValuePair("tipo", (String) ubicacion.get("tipo")));
//        params.add(new BasicNameValuePair("fechaActual", (String) ubicacion.get("fechaActual")));
//        params.add(new BasicNameValuePair("fecha", (String) ubicacion.get("fecha")));
//        params.add(new BasicNameValuePair("hora", (String) ubicacion.get("hora")));
        params.add(new BasicNameValuePair("mensajeHL7", s));

        // getting JSON Object
        JSONObject json = jsonParser.getJSONFromUrl(registerURL, params);
        // return json
        return json;
    }
//    
//    public JSONObject eliminarUbicacion(ContentValues ubicacion){
//        // Building Parameters
//    	
//        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        params.add(new BasicNameValuePair("tag", delete_tag));
//        params.add(new BasicNameValuePair("uid", (String)ubicacion.get("uid")));
//        params.add(new BasicNameValuePair("imei", (String)ubicacion.get("imei")));
//        // getting JSON Object
//        JSONObject json = jsonParser.getJSONFromUrl(registerURL, params);
//        // return json
//        return json;
//    }
//     
//    /**
//     * Function get Login status
//     * */
//     
//    public JSONObject updateStatus(ContentValues ubicacion){
//        // Building Parameters
//        List<NameValuePair> params = new ArrayList<NameValuePair>();
//        params.add(new BasicNameValuePair("tag", twitter_tag));
//        params.add(new BasicNameValuePair("uid", (String)ubicacion.get("uid")));
//        params.add(new BasicNameValuePair("status", (String)ubicacion.get("status")));        
//        JSONObject json = jsonParser.getJSONFromUrl(loginURL, params);
//        // return json
//        // Log.e("JSON", json.toString());
//        return json;
//    }
}